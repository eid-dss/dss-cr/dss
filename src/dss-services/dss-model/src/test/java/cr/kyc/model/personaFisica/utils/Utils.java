/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.kyc.model.personaFisica.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;

/**
 *
 * @author esolis
 */
public class Utils {
    
    public static <T> List<String[]> GetInvalidProperties(Set<ConstraintViolation<T>> cv) {
        
        List<String[]> invalidFields = new ArrayList<String[]>();
        Iterator<ConstraintViolation<T>> iterator = cv.iterator();
        while (iterator.hasNext()) {
            ConstraintViolation<T> i = iterator.next();
            String property = i.getPropertyPath().toString();
            String message = i.getMessage();
            invalidFields.add(new String[]{property, message});
        }
        
        return invalidFields;
    }
}
