/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author esolis
 */
@Entity
@Table(name = "EVENT_DETAIL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EventDetail.findAll", query = "SELECT e FROM EventDetail e"),
    @NamedQuery(name = "EventDetail.findByIdEventDetail", query = "SELECT e FROM EventDetail e WHERE e.idEventDetail = :idEventDetail")})
public class EventDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "event_detail_generator", sequenceName = "event_detail_sequence", allocationSize = 1)
    @GeneratedValue(generator = "event_detail_generator")
    @Column(name = "ID_EVENT_DETAIL")
    private Long idEventDetail;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "REQUEST")
    private String request;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "RESPONSE")
    private String response;
    @JoinColumn(name = "ID_EVENT_DETAIL", referencedColumnName = "ID_EVENT", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Event event;

    public EventDetail() {
    }

    public EventDetail(String request, String response) {
        this.request = request;
        this.response = response;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.idEventDetail);
        hash = 53 * hash + Objects.hashCode(this.request);
        hash = 53 * hash + Objects.hashCode(this.response);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EventDetail other = (EventDetail) obj;
        if (!Objects.equals(this.idEventDetail, other.idEventDetail)) {
            return false;
        }
        if (!Objects.equals(this.request, other.request)) {
            return false;
        }
        if (!Objects.equals(this.response, other.response)) {
            return false;
        }
        if (!Objects.equals(this.event, other.event)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EventDetail{" + "idEventDetail=" + idEventDetail + ", request=" + request + ", response=" + response + ", event=" + event + '}';
    }

}
