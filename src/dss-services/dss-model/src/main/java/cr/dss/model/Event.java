/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.model;


import cr.dss.model.IEvent.EventType;
import cr.dss.model.IEvent.StatusType;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author esolis
 */
@Entity
@Table(name = "EVENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Event.findAll", query = "SELECT e FROM Event e"),
    @NamedQuery(name = "Event.findByIdEvent", query = "SELECT e FROM Event e WHERE e.idEvent = :idEvent"),
    @NamedQuery(name = "Event.findByEvent", query = "SELECT e FROM Event e WHERE e.event_db = :event"),
    @NamedQuery(name = "Event.findByIdTransaction", query = "SELECT e FROM Event e WHERE e.idTransaction = :idTransaction"),
    @NamedQuery(name = "Event.findByBusinessApplication", query = "SELECT e FROM Event e WHERE e.businessApplication = :businessApplication"),
    @NamedQuery(name = "Event.findByStatus", query = "SELECT e FROM Event e WHERE e.status_db = :status"),
    @NamedQuery(name = "Event.findByCreationTime", query = "SELECT e FROM Event e WHERE e.creationTime = :creationTime")})
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "event_generator", sequenceName = "event_sequence", allocationSize = 1)
    @GeneratedValue(generator = "event_generator")
    @Column(name = "ID_EVENT")
    private Long idEvent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "EVENT")
    private String event_db;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "ID_TRANSACTION")
    private String idTransaction;
    @Size(max = 255)
    @Column(name = "BUSINESS_APPLICATION")
    private String businessApplication;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "STATUS")
    private String status_db;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATION_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "event")
    private EventDetail eventDetail;

    public Event() {
    }

    public Event(EventType event, String idTransaction, StatusType status, Date creationTime) {
        this.event_db = event.getValue();
        this.idTransaction = idTransaction;
        this.status_db = status.getValue();
        this.creationTime = creationTime;
    }

    public Event(EventType event, String idTransaction, String businessApplication, StatusType status, Date creationTime) {
        this.event_db = event.getValue();
        this.idTransaction = idTransaction;
        this.businessApplication = businessApplication;
        this.status_db = status.getValue();
        this.creationTime = creationTime;
    }

    public Long getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(Long idEvent) {
        this.idEvent = idEvent;
    }

    public EventType getEvent() {
        return IEvent.EventType.fromString(event_db);
    }

    public void setEvent(EventType event) {
        this.event_db = event.getValue();
    }

    public String getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(String idTransaction) {
        this.idTransaction = idTransaction;
    }

    public String getBusinessApplication() {
        return businessApplication;
    }

    public void setBusinessApplication(String businessApplication) {
        this.businessApplication = businessApplication;
    }

    public StatusType getStatus() {
        return IEvent.StatusType.fromString(status_db);
    }

    public void setStatus(StatusType status) {
        this.status_db = status.getValue();
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public EventDetail getEventDetail() {
        return eventDetail;
    }

    public void setEventDetail(EventDetail eventDetail) {
        this.eventDetail = eventDetail;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.idEvent);
        hash = 89 * hash + Objects.hashCode(this.event_db);
        hash = 89 * hash + Objects.hashCode(this.idTransaction);
        hash = 89 * hash + Objects.hashCode(this.businessApplication);
        hash = 89 * hash + Objects.hashCode(this.status_db);
        hash = 89 * hash + Objects.hashCode(this.creationTime);
        hash = 89 * hash + Objects.hashCode(this.eventDetail);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Event other = (Event) obj;
        if (!Objects.equals(this.idEvent, other.idEvent)) {
            return false;
        }
        if (!Objects.equals(this.event_db, other.event_db)) {
            return false;
        }
        if (!Objects.equals(this.idTransaction, other.idTransaction)) {
            return false;
        }
        if (!Objects.equals(this.businessApplication, other.businessApplication)) {
            return false;
        }
        if (!Objects.equals(this.status_db, other.status_db)) {
            return false;
        }
        if (!Objects.equals(this.creationTime, other.creationTime)) {
            return false;
        }
        if (!Objects.equals(this.eventDetail, other.eventDetail)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Event{" + "idEvent=" + idEvent + ", event=" + event_db + ", idTransaction=" + idTransaction + ", businessApplication=" + businessApplication + ", status=" + status_db + ", creationTime=" + creationTime + ", eventDetail=" + eventDetail + '}';
    }

}
