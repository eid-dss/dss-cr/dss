package cr.test.dssAuthentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DssAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(DssAuthenticationApplication.class, args);
	}
}
