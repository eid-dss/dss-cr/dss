/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.test.dssAuthentication.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author esolis
 */
@Controller
public class LoginController {

    /**
     *
     * @param model The model attributes
     * @return The view name.
     */
    @RequestMapping(value = "/login")
    public final String showLogin(final Model model) {
        return "login";
    }
}
