/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.dss.lotl.util.web.controller;

import eu.europa.esig.dss.tsl.service.TSLRepository;
import eu.europa.esig.dss.utils.Utils;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author esolis
 */
@Controller
@RequestMapping(value = {"/", "/tsl-info"})
public class TrustedListController {

    private static final Logger LOG = Logger.getLogger(TrustedListController.class.getName());

    @Autowired
    private TSLRepository tslRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String getSummary(final Model model) {
        model.addAttribute("summary", tslRepository.getSummary());
        return "tsl-info";
    }

    @RequestMapping(value = "/{country:[a-z][a-z]}", method = RequestMethod.GET)
    public String getByCountry(@PathVariable String country, Model model) {
        String countryUppercase = Utils.upperCase(country);
        model.addAttribute("country", countryUppercase);
        model.addAttribute("countries", tslRepository.getAllMapTSLValidationModels().keySet());
        model.addAttribute("model", tslRepository.getByCountry(countryUppercase));
        return "tsl-info-country";
    }
}
