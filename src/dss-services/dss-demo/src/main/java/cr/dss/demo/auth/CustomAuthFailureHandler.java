/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.auth;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

/**
 *
 * @author esolis
 */
@Component
public class CustomAuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    private static final Logger LOG = Logger.getLogger(CustomAuthFailureHandler.class.getName());

    private static final String URL_FAILURE_REDIRECT = "/login?error";

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException {

        LOG.log(Level.INFO, "*** CustomAuthFailureHandler ***");

        if (exception.getClass().isAssignableFrom(
                BadCredentialsException.class)) {

            LOG.log(Level.INFO, "*** CustomAuthFailureHandler - BadCredentialsException ***");

        }

        getRedirectStrategy().sendRedirect(request, response, URL_FAILURE_REDIRECT);

    }

}
