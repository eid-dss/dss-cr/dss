/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.service.client;

import cr.dss.demo.model.GenderTypeResponse;
import cr.dss.demo.model.IdentificationTypeResponse;
import cr.dss.demo.model.KycProvince;
import cr.dss.demo.model.MaritalStatusResponse;
import cr.dss.demo.model.Response;
import cr.dss.demo.model.SourceIncomeResponse;
import cr.dss.demo.model.ValidateRequest;
import cr.dss.demo.model.ValidateResponse;
import cr.dss.demo.model.DocumentTypeResponse;
import cr.dss.demo.model.ValidateDocumentRequest;
import cr.dss.demo.model.ValidateDocumentResponse;
import cr.kyc.model.IKyc;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 *
 * @author esolis
 */
@Component
public class DssService {

    private static final Logger LOG = Logger.getLogger(DssService.class.getName());

    private static final String DSS_SERVICES_URL = "http://127.0.0.1:8095/";

    public ValidateDocumentResponse validateDocument(ValidateDocumentRequest validateDocumentRequest) throws Exception {

        LOG.log(Level.INFO, "*** ValidateDocument ***");

        String url = DSS_SERVICES_URL + "rest/dss/validate-document";

        // Set the Content-Type header
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<ValidateDocumentRequest> requestEntity = new HttpEntity<>(validateDocumentRequest, requestHeaders);

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        ParameterizedTypeReference<Response<ValidateDocumentResponse>> responseType = new ParameterizedTypeReference<Response<ValidateDocumentResponse>>() {
        };

        try {
            ResponseEntity<Response<ValidateDocumentResponse>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, responseType);

            Response response = responseEntity.getBody();

            if (response.isSuccess()) {

                ValidateDocumentResponse validateDocumentResponse = (ValidateDocumentResponse) response.getResponse();

                return validateDocumentResponse;
            } else {
                throw new Exception(response.getErrorMessage());
            }

        } catch (HttpClientErrorException e) {
            throw e;
        }

    }

}
