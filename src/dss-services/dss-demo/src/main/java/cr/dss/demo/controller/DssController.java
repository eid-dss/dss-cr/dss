/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.controller;

import cr.dss.demo.model.Document;
import cr.dss.demo.model.SignatureParameters;
import java.util.logging.Logger;

import eu.europa.esig.dss.utils.Utils;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cr.dss.demo.model.SignatureProcess;
import cr.dss.demo.model.ValidateDocumentRequest;
import cr.dss.demo.service.client.DssService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author esolis
 */
@Controller
@RequestMapping("/dss")
public class DssController {

    private static final Logger LOG = Logger.getLogger(DssController.class.getName());

    @Autowired
    DssService dssService;

    private SignatureParameters signatureParameters;

    @GetMapping("/signatureProcess")
    public String signatureProcess(@Valid @ModelAttribute("signatureParameters") SignatureParameters signatureParameters, Model model) {

        this.signatureParameters = signatureParameters;

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

        model.addAttribute("signatureParameters", gson.toJson(signatureParameters));
        model.addAttribute("signatureProcess", new SignatureProcess());
//        LOG.log(Level.INFO, "Document to sign: {0}", documentToSign);
        model.addAttribute("documentToSign", new String(Utils.fromBase64(signatureParameters.getDocument().getEncodedDocument())));

        return "dss/signatureProcess";
    }

    @PostMapping(value = "/signatureProcess", params = {"download"})
    public String downloadSignedFile(@ModelAttribute("signatureProcess") SignatureProcess signatureProcess, Model model, HttpServletResponse response) throws IOException {

        LOG.log(Level.INFO, "signed document: {0}", signatureProcess.getSignedDocument());

        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + "signed_" + signatureParameters.getDocument().getName() + "\"");
        Utils.copy(new ByteArrayInputStream(Utils.fromBase64(signatureProcess.getSignedDocument())), response.getOutputStream());

        return null;
    }

    @PostMapping(value = "/signatureProcess", params = {"validate"})
    public String validateSignedFile(@ModelAttribute("signatureProcess") SignatureProcess signatureProcess, 
            Model model, final RedirectAttributes redirectAttributes) throws IOException, Exception {

        Document document = new Document();
        document.setEncodedDocument(signatureProcess.getSignedDocument());
        document.setMimeType(signatureParameters().getDocument().getMimeType());
        document.setName("signed_" + signatureParameters.getDocument().getName());
        
        ValidateDocumentRequest validateDocumentRequest = new ValidateDocumentRequest();
        validateDocumentRequest.setSignedDocument(document);
        validateDocumentRequest.setOriginalDocument(signatureParameters().getDocument());

        redirectAttributes.addFlashAttribute("validateDocumentRequest", validateDocumentRequest);

        return "redirect:/validation";
    }

    @ModelAttribute("signatureParameters")
    public SignatureParameters signatureParameters() throws Exception {

        return signatureParameters;

    }

}
