/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.auth;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 *
 * @author esolis
 */
public class AuthFilter extends UsernamePasswordAuthenticationFilter {

    private static final Logger LOG = Logger.getLogger(AuthFilter.class.getName());

    private static final String SPRING_SECURITY_FORM_TOKEN_KEY = "token";
    private static final String SPRING_SECURITY_FORM_HTTP_METOD = "POST";

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (!request.getMethod().equals(SPRING_SECURITY_FORM_HTTP_METOD)) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }

        LOG.log(Level.INFO, "*** AuthFilter ***");

        LOG.log(Level.INFO, "Token: {0}", obtainToken(request));

        String token = obtainToken(request);
        String remoteHost = request.getRemoteHost();
        String remoteAddr = request.getRemoteAddr();

        if (token == null) {
            token = "";
        }

        AuthenticationToken scaAuthenticationToken = new AuthenticationToken(token);
        setDetails(request, scaAuthenticationToken);
        return getAuthenticationManager().authenticate(scaAuthenticationToken);
    }

    protected void setDetails(HttpServletRequest request,
            AuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }

    protected String obtainToken(HttpServletRequest request) {
        return request.getParameter(SPRING_SECURITY_FORM_TOKEN_KEY);
    }

}
