/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cr.dss.demo.model.Document;
import cr.dss.demo.model.SignatureParameters;
import eu.europa.esig.dss.BLevelParameters;
import eu.europa.esig.dss.DigestAlgorithm;
import eu.europa.esig.dss.MimeType;
import eu.europa.esig.dss.SignatureLevel;
import eu.europa.esig.dss.SignaturePackaging;
import eu.europa.esig.dss.TimestampParameters;
import eu.europa.esig.dss.utils.Utils;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author esolis
 */
@Controller
public class LoginController {

    private static final Logger LOG = Logger.getLogger(LoginController.class.getName());

    SignatureParameters signatureParameters = new SignatureParameters();

    @GetMapping("/login")
    public String showLogin(Map<String, Object> model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        LOG.log(Level.INFO, "getPrincipal: {0}", auth.getPrincipal());

        if (!auth.getPrincipal().equals("anonymousUser")) {
            return "redirect:/";
        } else {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

            model.put("message", "Hello World");

            Document document = new Document();

            BLevelParameters bLevelParameters = new BLevelParameters();

            TimestampParameters timestampParameters = new TimestampParameters();

            document.setEncodedDocument(Utils.toBase64("aa".getBytes()));
            document.setMimeType(MimeType.PKCS7);

            bLevelParameters.setSigningDate(new Date());
            timestampParameters.setDigestAlgorithm(DigestAlgorithm.SHA512);

            signatureParameters.setSignWithExpiredCertificate(true);
            signatureParameters.setSignatureLevel(SignatureLevel.CAdES_BASELINE_B);
            signatureParameters.setSignaturePackaging(SignaturePackaging.ENVELOPING);
            signatureParameters.setDigestAlgorithm(DigestAlgorithm.SHA512);
            signatureParameters.setBLevelParams(bLevelParameters);

            signatureParameters.setContentTimestampParameters(timestampParameters);
            signatureParameters.setSignatureTimestampParameters(timestampParameters);
            signatureParameters.setArchiveTimestampParameters(timestampParameters);
            signatureParameters.setDocument(document);

            model.put("signatureParameters", gson.toJson(signatureParameters));

            return "login";
        }

    }

}
