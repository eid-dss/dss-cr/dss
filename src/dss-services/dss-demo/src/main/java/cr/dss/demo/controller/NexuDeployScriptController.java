/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import freemarker.template.Configuration;
import freemarker.template.Template;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @author esolis
 */
@PropertySource("classpath:dss.properties")
@Controller
public class NexuDeployScriptController {
    
    private static final Logger LOG = Logger.getLogger(NexuDeployScriptController.class.getName());

    @Value("${baseUrl}")
    private String baseUrl;

    @Value("${nexuVersion}")
    private String nexuVersion;

    @Value("${nexuUrl}")
    private String nexuUrl;

    private Template template;

    public NexuDeployScriptController() {
        try {
            Configuration cfg = new Configuration(Configuration.VERSION_2_3_22);
            cfg.setClassForTemplateLoading(getClass(), "/");
            this.template = cfg.getTemplate("nexu_deploy.ftl.js", "UTF-8");
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @RequestMapping("/js/nexu-deploy.js")
    public ResponseEntity<String> loadScript() throws Exception {

        StringWriter outWriter = new StringWriter();

        Map<String, String> model = new HashMap<>();

        model.put("baseUrl", baseUrl);
        model.put("nexuVersion", nexuVersion);
        model.put("nexuUrl", nexuUrl);

        template.process(model, outWriter);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("text/javascript"));

        return new ResponseEntity<>(outWriter.toString(), headers, HttpStatus.ACCEPTED);
    }

}
