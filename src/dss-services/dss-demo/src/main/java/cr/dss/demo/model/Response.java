/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 *
 * @author esolis
 * @param <T>
 */
@JsonInclude(Include.NON_EMPTY)
public class Response<T> {

    private boolean success;
    private String error;
    private String errorMessage;
    private T response;
    private Feedback feedback;

    public Response() {
    }

    public Response(T response) {
        this.success = true;
        this.response = response;
    }

    public Response(T response, String error, String errorMessage, Feedback feedback) {
        this.success = false;
        this.response = response;
        this.error = error;
        this.errorMessage = errorMessage;
        this.feedback = feedback;
    }

    public boolean isSuccess() {
        return success;
    }

    public T getResponse() {
        return response;
    }

    public String getError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    @Override
    public String toString() {
        return "Response{" + "success=" + success + ", error=" + error + ", errorMessage=" + errorMessage + ", response=" + response + ", feedback=" + feedback + '}';
    }

}
