/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.auth;

import cr.dss.demo.model.Document;
import cr.dss.demo.model.ValidateDocumentRequest;
import cr.dss.demo.model.ValidateDocumentResponse;
import cr.dss.demo.service.client.DssService;
import eu.europa.esig.dss.MimeType;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

/**
 *
 * @author esolis
 */
@Component
public class AuthProvider implements AuthenticationProvider {

    private static final Logger logger = Logger.getLogger(AuthProvider.class.getName());

    @Autowired
    DssService dssService;

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        final AuthenticationToken tokenContainer = (AuthenticationToken) auth;
        final String token = tokenContainer.getToken();

        Document document = new Document();
        document.setEncodedDocument(token);
        document.setMimeType(MimeType.PKCS7);

        List<GrantedAuthority> grantedAuths = new ArrayList<>();

        logger.log(Level.INFO, "*** Creating token ***");

        logger.log(Level.INFO, "Token: {0}", token);

        if (token.isEmpty()) {
            logger.log(Level.INFO, "BadCredentialsException");
            throw new BadCredentialsException("External system authentication failed");
        } else {
            try {
                ValidateDocumentRequest validateDocumentRequest = new ValidateDocumentRequest();
                validateDocumentRequest.setSignedDocument(document);
//                Thread.sleep(90000);
                ValidateDocumentResponse validateDocumentResponse = dssService.validateDocument(validateDocumentRequest);

                logger.log(Level.INFO, validateDocumentResponse.toString());

                long signingDate = validateDocumentResponse.getDiagnosticData().getSignatures().get(0).getDateTime().getTime();
                long validationDate = validateDocumentResponse.getDiagnosticData().getValidationDate().getTime();

                long signatureAndValidationDiferenceInMinutes = (int) ((validationDate - signingDate) / (86400.0 * 1000.0));

                logger.log(Level.INFO, "Time to sign and validate: {0}", signatureAndValidationDiferenceInMinutes);

//              La firma es valida?
                if (!validateDocumentResponse.getDiagnosticData().getSignatures().isEmpty()) {
                    if (validateDocumentResponse.getDiagnosticData().getSignatures().get(0).getBasicSignature().isSignatureValid()) {

                        if (signatureAndValidationDiferenceInMinutes <= 1) {
                            String user = validateDocumentResponse.getSimpleReport().getSignature().get(0).getSignedBy();

                            logger.log(Level.INFO, "User authenticated: {0}", user);

                            grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));
                            return new AuthenticationToken(user, token, grantedAuths);
                        } else {
                            throw new BadCredentialsException("External system authentication failed - Time between signing and validation mayor to 1 minute");
                        }
                    } else {
                        throw new BadCredentialsException("External system authentication failed - Signature validation failed");
                    }
                } else {
                    throw new BadCredentialsException("External system authentication failed - No signed token");
                }

            } catch (Exception ex) {
//                Logger.getLogger(AuthProvider.class.getName()).log(Level.SEVERE, null, ex);
                throw new BadCredentialsException("External system authentication failed");
            }
        }

//        //do i know this token?
//        if (!tokenStore.contains(token)) {
//            //...if not: the token is invalid!
//            throw new BadCredentialsException("Invalid token - " + token);
//        }
//
//        final String username = tokenStore.get(token);
//        if (!userStore.contains(username)) {
//            //normally this shouldn't be happen
//            throw new BadCredentialsException("No user found for token - " + token);
//        }
//
//        final User user = userStore.get(username);
    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(AuthenticationToken.class);
    }

}
