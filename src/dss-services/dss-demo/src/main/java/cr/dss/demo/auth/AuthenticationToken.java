/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.auth;

import java.util.Collection;
import java.util.Collections;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author esolis
 */
public class AuthenticationToken extends AbstractAuthenticationToken {

    private final String token;
    private final String user;

    public AuthenticationToken(String token) {
        super(Collections.emptyList());
        this.user = "";
        this.token = token;
        setAuthenticated(false);
    }

    public AuthenticationToken(String user, String token, Collection<? extends GrantedAuthority> authorities) {
        //note that the constructor needs a collection of GrantedAuthority
        //but our User have a collection of our UserRole's
        super(authorities);

        this.token = token;
        this.user = user;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return getToken();
    }

    @Override
    public Object getPrincipal() {
        return getUser();
    }

    public String getToken() {
        return token;
    }

    public String getUser() {
        return user;
    }

}
