/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

/**
 *
 * @author esolis
 */
public class SignatureProcess {

    private String signedDocument;

    public String getSignedDocument() {
        return signedDocument;
    }

    public void setSignedDocument(String signedDocument) {
        this.signedDocument = signedDocument;
    }

    @Override
    public String toString() {
        return "SignatureProcess{" +  "signedDocument=" + signedDocument + '}';
    }

}
