/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.controller;

import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import cr.dss.demo.model.ValidateDocumentRequest;
import cr.dss.demo.model.ValidateDocumentResponse;
import cr.dss.demo.service.client.DssService;
import cr.kyc.utils.Utils;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author esolis
 */
@Controller
@RequestMapping("/validation")
public class ValidationController {

    private static final Logger LOG = Logger.getLogger(ValidationController.class.getName());

    @Autowired
    DssService dssService;

    private ValidateDocumentRequest validateDocumentRequest;

    @GetMapping()
    public String showValidationForm(@ModelAttribute("validateDocumentRequest") ValidateDocumentRequest validateDocumentRequest, Model model) {

        setValidateDocumentRequest(validateDocumentRequest);

        LOG.log(Level.INFO, "validateDocumentRequest: {0}", validateDocumentRequest);

        model.addAttribute("documentToValidate", new String(Utils.fromBase64(validateDocumentRequest.getSignedDocument().getEncodedDocument())));

        model.addAttribute("validateDocumentRequest", validateDocumentRequest);

        return "dss/validation";
    }

    @PostMapping()
    public String validate(Model model) throws IOException, Exception {

        ValidateDocumentResponse validateDocumentResponse = dssService.validateDocument(getValidateDocumentRequest());

        model.addAttribute("simpleReport", validateDocumentResponse.getSimpleReport());
        model.addAttribute("diagnosticData", validateDocumentResponse.getDiagnosticData());
        model.addAttribute("detailedReport", validateDocumentResponse.getDetailedReport());
        
        return "dss/validation";
    }

    private ValidateDocumentRequest getValidateDocumentRequest() {
        return validateDocumentRequest;
    }

    private void setValidateDocumentRequest(ValidateDocumentRequest validateDocumentRequest) {
        this.validateDocumentRequest = validateDocumentRequest;
    }

}
