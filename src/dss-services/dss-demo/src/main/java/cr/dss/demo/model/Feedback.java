/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

/**
 *
 * @author esolis
 */
public class Feedback {

    private final FeedbackStatus feedbackStatus;
    private final String stacktrace;

    public Feedback(FeedbackStatus feedbackStatus, String stacktrace) {
        this.feedbackStatus = feedbackStatus;
        this.stacktrace = stacktrace;
    }

    public FeedbackStatus getFeedbackStatus() {
        return feedbackStatus;
    }

    public String getStacktrace() {
        return stacktrace;
    }
    
    
}
