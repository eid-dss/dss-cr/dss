/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import cr.kyc.model.IKyc;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author esolis
 */
public class DocumentTypeResponse implements Serializable{
    
        private List<IKyc.DocumentType> documentTypeList;

    public List<IKyc.DocumentType> getDocumentTypeList() {
        return documentTypeList;
    }

    public void setDocumentTypeList(List<IKyc.DocumentType> documentTypeList) {
        this.documentTypeList = documentTypeList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.documentTypeList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DocumentTypeResponse other = (DocumentTypeResponse) obj;
        if (!Objects.equals(this.documentTypeList, other.documentTypeList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DocumentTypeResponse{" + "documentTypeList=" + documentTypeList + '}';
    }
    
}
