/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 *
 * @author esolis
 */
public class ValidateRequest implements Serializable {

    @NotNull (message = "{poc.kyc.model.KycValidateRequest.type.NotNull}")
    private String type;

    @NotNull (message = "{poc.kyc.model.KycValidateRequest.document.NotNull}")
    private String document;

    public ValidateRequest() {
    }

    public ValidateRequest(String type, String document) {
        this.type = type;
        this.document = document;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.type);
        hash = 23 * hash + Objects.hashCode(this.document);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ValidateRequest other = (ValidateRequest) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.document, other.document)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ValidateRequest{" + "type=" + type + ", document=" + document + '}';
    }

}
