/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 *
 * @author esolis
 */

public class ValidateResponse extends ValidateRequest{

    @NotNull(message = "{poc.kyc.model.PersonaFisica.tipoIdentificacion.NotNull}")
    private String status;
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ValidateResponse other = (ValidateResponse) obj;
        if (!Objects.equals(this.status, other.status)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "KycResponse{" + super.toString() + "status=" + status + '}';
    }

}
