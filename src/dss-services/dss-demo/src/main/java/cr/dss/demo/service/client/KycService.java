/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.service.client;

import cr.dss.demo.model.GenderTypeResponse;
import cr.dss.demo.model.IdentificationTypeResponse;
import cr.dss.demo.model.KycProvince;
import cr.dss.demo.model.MaritalStatusResponse;
import cr.dss.demo.model.Response;
import cr.dss.demo.model.SourceIncomeResponse;
import cr.dss.demo.model.ValidateRequest;
import cr.dss.demo.model.ValidateResponse;
import cr.dss.demo.model.DocumentTypeResponse;
import cr.kyc.model.IKyc;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 *
 * @author esolis
 */
@Component
public class KycService {

    private static final Logger LOG = Logger.getLogger(KycService.class.getName());

    private static final String APP_URL = "http://localhost:8090";

    public ValidateResponse validateDocument(String type, String document) throws Exception {

        LOG.log(Level.INFO, "*** ValidateDocument ***");
        
        String url = APP_URL + "/rest/kyc/validate";

        ValidateRequest validateRequest = new ValidateRequest();
        validateRequest.setType(type);
        validateRequest.setDocument(document);

        // Set the Content-Type header
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<ValidateRequest> requestEntity = new HttpEntity<>(validateRequest, requestHeaders);

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        ParameterizedTypeReference<Response<ValidateResponse>> responseType = new ParameterizedTypeReference<Response<ValidateResponse>>() {
        };

        try {
            ResponseEntity<Response<ValidateResponse>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, responseType);

            Response response = responseEntity.getBody();

            if (response.isSuccess()) {

                ValidateResponse validateResponse = (ValidateResponse) response.getResponse();

                return validateResponse;
            } else {
                throw new Exception(response.getErrorMessage());
            }

        } catch (HttpClientErrorException e) {
            throw e;
        }

    }

    public DocumentTypeResponse documentTypeResponse() throws HttpClientErrorException, Exception {

        LOG.log(Level.INFO, "*** documentTypeResponse ***");

        String url = APP_URL + "/rest/kyc/util/documentType";

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        ParameterizedTypeReference<Response<DocumentTypeResponse>> responseType = new ParameterizedTypeReference<Response<DocumentTypeResponse>>() {
        };

        try {
            ResponseEntity<Response<DocumentTypeResponse>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, responseType);

            Response response = responseEntity.getBody();

            if (response.isSuccess()) {

                DocumentTypeResponse documentTypeResponse = (DocumentTypeResponse) response.getResponse();

                return documentTypeResponse;
            } else {
                throw new Exception(response.getErrorMessage());
            }

        } catch (HttpClientErrorException e) {
            throw e;
        }

    }

    public IdentificationTypeResponse identificationTypeResponse() throws HttpClientErrorException, Exception {

        LOG.log(Level.INFO, "*** identificationTypeResponse ***");

        String url = APP_URL + "/rest/kyc/util/identificationType";

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        ParameterizedTypeReference<Response<IdentificationTypeResponse>> responseType = new ParameterizedTypeReference<Response<IdentificationTypeResponse>>() {
        };

        try {
            ResponseEntity<Response<IdentificationTypeResponse>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, responseType);

            Response response = responseEntity.getBody();

            if (response.isSuccess()) {

                IdentificationTypeResponse identificationTypeResponse = (IdentificationTypeResponse) response.getResponse();

                return identificationTypeResponse;
            } else {
                throw new Exception(response.getErrorMessage());
            }

        } catch (HttpClientErrorException e) {
            throw e;
        }

    }

    public GenderTypeResponse genderTypeResponse() throws HttpClientErrorException, Exception {

        LOG.log(Level.INFO, "*** genderTypeResponse ***");

        String url = APP_URL + "/rest/kyc/util/genderType";

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        ParameterizedTypeReference<Response<GenderTypeResponse>> responseType = new ParameterizedTypeReference<Response<GenderTypeResponse>>() {
        };

        try {
            ResponseEntity<Response<GenderTypeResponse>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, responseType);

            Response response = responseEntity.getBody();

            if (response.isSuccess()) {

                GenderTypeResponse genderTypeResponse = (GenderTypeResponse) response.getResponse();

                return genderTypeResponse;
            } else {
                throw new Exception(response.getErrorMessage());
            }

        } catch (HttpClientErrorException e) {
            throw e;
        }

    }

    public MaritalStatusResponse maritalStatusResponse() throws HttpClientErrorException, Exception {

        LOG.log(Level.INFO, "*** maritalStatusResponse ***");

        String url = APP_URL + "/rest/kyc/util/maritalStatus";

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        ParameterizedTypeReference<Response<MaritalStatusResponse>> responseType = new ParameterizedTypeReference<Response<MaritalStatusResponse>>() {
        };

        try {
            ResponseEntity<Response<MaritalStatusResponse>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, responseType);

            Response response = responseEntity.getBody();

            if (response.isSuccess()) {

                MaritalStatusResponse maritalStatusResponse = (MaritalStatusResponse) response.getResponse();

                return maritalStatusResponse;
            } else {
                throw new Exception(response.getErrorMessage());
            }

        } catch (HttpClientErrorException e) {
            throw e;
        }

    }

    public SourceIncomeResponse sourceIncomeResponse() throws HttpClientErrorException, Exception {

        LOG.log(Level.INFO, "*** sourceIncomeResponse ***");

        String url = APP_URL + "/rest/kyc/util/sourceIncome";

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        ParameterizedTypeReference<Response<SourceIncomeResponse>> responseType = new ParameterizedTypeReference<Response<SourceIncomeResponse>>() {
        };

        try {
            ResponseEntity<Response<SourceIncomeResponse>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, responseType);

            Response response = responseEntity.getBody();

            if (response.isSuccess()) {

                SourceIncomeResponse sourceIncomeResponse = (SourceIncomeResponse) response.getResponse();

                return sourceIncomeResponse;
            } else {
                throw new Exception(response.getErrorMessage());
            }

        } catch (HttpClientErrorException e) {
            throw e;
        }

    }

    public List<KycProvince> provinceListResponse() throws HttpClientErrorException, Exception {

        LOG.log(Level.INFO, "*** provinceListResponse ***");

        String url = APP_URL + "/rest/kyc/util/province";

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        ParameterizedTypeReference<Response<List<KycProvince>>> responseType = new ParameterizedTypeReference<Response<List<KycProvince>>>() {
        };

        try {
            ResponseEntity<Response<List<KycProvince>>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, responseType);

            Response response = responseEntity.getBody();

            if (response.isSuccess()) {

                List<KycProvince> provinceList = (List<KycProvince>) response.getResponse();

                return provinceList;
            } else {
                throw new Exception(response.getErrorMessage());
            }

        } catch (HttpClientErrorException e) {
            throw e;
        }

    }
}
