/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import cr.kyc.model.naturalPerson.INaturalPerson;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author esolis
 */
public class MaritalStatusResponse implements Serializable{
    
    private List<INaturalPerson.MaritalStatusType> maritalStatusList;

    public List<INaturalPerson.MaritalStatusType> getMaritalStatusList() {
        return maritalStatusList;
    }

    public void setMaritalStatusList(List<INaturalPerson.MaritalStatusType> maritalStatusList) {
        this.maritalStatusList = maritalStatusList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.maritalStatusList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MaritalStatusResponse other = (MaritalStatusResponse) obj;
        if (!Objects.equals(this.maritalStatusList, other.maritalStatusList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MaritalStatusResponse{" + "maritalStatusList=" + maritalStatusList + '}';
    }
    
}
