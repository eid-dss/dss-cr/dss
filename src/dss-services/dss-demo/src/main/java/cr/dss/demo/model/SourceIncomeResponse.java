/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import cr.kyc.model.naturalPerson.INaturalPerson;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author esolis
 */
public class SourceIncomeResponse implements Serializable{
    
    private List<INaturalPerson.SourceIncomeType> sourceIncomeList;

    public List<INaturalPerson.SourceIncomeType> getSourceIncomeList() {
        return sourceIncomeList;
    }

    public void setSourceIncomeList(List<INaturalPerson.SourceIncomeType> sourceIncomeList) {
        this.sourceIncomeList = sourceIncomeList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.sourceIncomeList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SourceIncomeResponse other = (SourceIncomeResponse) obj;
        if (!Objects.equals(this.sourceIncomeList, other.sourceIncomeList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SourceIncomeResponse{" + "sourceIncomeList=" + sourceIncomeList + '}';
    }
    
}
