/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author esolis
 */
public class KycProvince implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String auditCreationUsr;
    private Date auditCreationTs;
    private String auditModificationUsr;
    private Date auditModificationTs;
    private int optLock;

    public KycProvince() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuditCreationUsr() {
        return auditCreationUsr;
    }

    public void setAuditCreationUsr(String auditCreationUsr) {
        this.auditCreationUsr = auditCreationUsr;
    }

    public Date getAuditCreationTs() {
        return auditCreationTs;
    }

    public void setAuditCreationTs(Date auditCreationTs) {
        this.auditCreationTs = auditCreationTs;
    }

    public String getAuditModificationUsr() {
        return auditModificationUsr;
    }

    public void setAuditModificationUsr(String auditModificationUsr) {
        this.auditModificationUsr = auditModificationUsr;
    }

    public Date getAuditModificationTs() {
        return auditModificationTs;
    }

    public void setAuditModificationTs(Date auditModificationTs) {
        this.auditModificationTs = auditModificationTs;
    }

    public int getOptLock() {
        return optLock;
    }

    public void setOptLock(int optLock) {
        this.optLock = optLock;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KycProvince)) {
            return false;
        }
        KycProvince other = (KycProvince) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.kyc.jpa.domain.KycProvince[ id=" + id + " ]";
    }

}
