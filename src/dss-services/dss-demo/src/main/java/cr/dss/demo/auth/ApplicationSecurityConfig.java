/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 *
 * @author esolis
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    AuthProvider authProvider;

    @Autowired
    CustomAuthFailureHandler customAuthFailureHandler;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/webjars/**");
    }

    @Bean
    public AuthFilter authenticationFilter() throws Exception {
        AuthFilter authFilter = new AuthFilter();
        authFilter.setAuthenticationManager(super.authenticationManager());
        authFilter.setAuthenticationFailureHandler(customAuthFailureHandler);
        return authFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off

        http
                .csrf().disable();

        http
                .addFilter(authenticationFilter())
                .authenticationProvider(authProvider)
                .authorizeRequests()
                .antMatchers("/resources/**", "/webjars/**", "/css/**", "/js/**", "/img/**").permitAll()
                .antMatchers("/").permitAll()
                //                .antMatchers("/webjars/**").permitAll()
                //                .antMatchers("/js/**").permitAll()
                //                .antMatchers("/css/**").permitAll()
                //                .requestMatchers(StaticResourceRequest.toCommonLocations()).permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").failureUrl("/login?error")
                .defaultSuccessUrl("/", true)
                .permitAll()
                //                .usernameParameter("token")
                .and()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                //                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login?logout")
                //                .logoutSuccessUrl("/login")
                .permitAll();
//        http://www.mkyong.com/spring-boot/spring-boot-spring-security-thymeleaf-example/
//                .and()
//        .exceptionHandling().accessDeniedHandler(accessDeniedHandler);

//        http
//                .sessionManagement()
//                .maximumSessions(1)
//                .expiredUrl("/login?expired")
//                .maxSessionsPreventsLogin(true)
//                .and()
//                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
//                .invalidSessionUrl("/");
        // @formatter:on
    }

}
