/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

/**
 *
 * @author esolis
 */
public enum FeedbackStatus {
    
    EXCEPTION, SUCCESS, FAILED;
}
