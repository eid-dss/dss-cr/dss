/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import cr.kyc.model.IKyc;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author esolis
 */
public class IdentificationTypeResponse implements Serializable{
    
    private List<IKyc.IdentificacionType> identificationTypeList;

    public List<IKyc.IdentificacionType> getIdentificationTypeList() {
        return identificationTypeList;
    }

    public void setIdentificationTypeList(List<IKyc.IdentificacionType> documentTypeList) {
        this.identificationTypeList = documentTypeList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.identificationTypeList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IdentificationTypeResponse other = (IdentificationTypeResponse) obj;
        if (!Objects.equals(this.identificationTypeList, other.identificationTypeList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "IdentificationTypeResponse{" + "identificationTypeList=" + identificationTypeList + '}';
    }
    
}
