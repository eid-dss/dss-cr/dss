/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import cr.kyc.model.naturalPerson.INaturalPerson;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author esolis
 */
public class GenderTypeResponse implements Serializable{
    
    private List<INaturalPerson.GenderType> genderTypeList;

    public List<INaturalPerson.GenderType> getGenderTypeList() {
        return genderTypeList;
    }

    public void setGenderTypeList(List<INaturalPerson.GenderType> genderTypeList) {
        this.genderTypeList = genderTypeList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.genderTypeList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GenderTypeResponse other = (GenderTypeResponse) obj;
        if (!Objects.equals(this.genderTypeList, other.genderTypeList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GenderTypeResponse{" + "genderTypeList=" + genderTypeList + '}';
    }
    
}
