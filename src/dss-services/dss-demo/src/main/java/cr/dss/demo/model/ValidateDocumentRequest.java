/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import cr.dss.demo.model.Document;
import eu.europa.esig.jaxb.policy.ConstraintsParameters;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 *
 * @author esolis
 */
public class ValidateDocumentRequest {
    
    @NotNull
    private Document signedDocument;
    
    @Null
    private Document originalDocument;
    
    @Null
    private ConstraintsParameters policy;

    public Document getSignedDocument() {
        return signedDocument;
    }

    public void setSignedDocument(Document signedDocument) {
        this.signedDocument = signedDocument;
    }

    public Document getOriginalDocument() {
        return originalDocument;
    }

    public void setOriginalDocument(Document originalDocument) {
        this.originalDocument = originalDocument;
    }

    public ConstraintsParameters getPolicy() {
        return policy;
    }

    public void setPolicy(ConstraintsParameters policy) {
        this.policy = policy;
    }

    @AssertTrue(message = "{error.to.sign.file.mandatory}")
    public boolean isOriginalDocument() {
        return (originalDocument != null) && (!originalDocument.getEncodedDocument().isEmpty());
    }

    @Override
    public String toString() {
        return "ValidateDocumentRequest{" + "signedDocument=" + signedDocument + ", originalDocument=" + originalDocument + ", policy=" + policy + '}';
    }

}
