/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.model;

import eu.europa.esig.dss.MimeType;
import java.io.Serializable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author esolis
 */
public class Document implements Serializable {

    @NotNull
    private String encodedDocument;

    @NotNull
    private MimeType mimeType;

    private String name;

    public String getEncodedDocument() {
        return encodedDocument;
    }

    public void setEncodedDocument(String encodedDocument) {
        this.encodedDocument = encodedDocument;
    }

    public MimeType getMimeType() {
        return mimeType;
    }

    public void setMimeType(MimeType mimeType) {
        this.mimeType = mimeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Document{" + "encodedDocument=" + encodedDocument + ", mimeType=" + mimeType + ", name=" + name + '}';
    }
}
