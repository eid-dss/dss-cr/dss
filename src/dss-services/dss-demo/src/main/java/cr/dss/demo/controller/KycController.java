/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.demo.controller;

import cr.dss.demo.model.Document;
import cr.dss.demo.model.KycProvince;
import cr.dss.demo.model.SignatureParameters;
import cr.dss.demo.model.ValidateResponse;
import cr.dss.demo.service.client.KycService;
import cr.kyc.converter.Converter;
import cr.kyc.converter.exceptions.ConversionException;
import cr.kyc.model.IKyc;
import cr.kyc.model.Nationality;
import cr.kyc.model.groups.OtherValidator;
import cr.kyc.model.naturalPerson.INaturalPerson;
import cr.kyc.model.naturalPerson.NaturalPerson;
import cr.kyc.model.naturalPerson.SourceIncome;
import eu.europa.esig.dss.BLevelParameters;
import eu.europa.esig.dss.DigestAlgorithm;
import eu.europa.esig.dss.MimeType;
import eu.europa.esig.dss.SignatureLevel;
import eu.europa.esig.dss.SignaturePackaging;
import eu.europa.esig.dss.TimestampParameters;
import eu.europa.esig.dss.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author esolis
 */
@Controller
@RequestMapping("/kyc")
public class KycController {

    private static final Logger LOG = Logger.getLogger(KycController.class.getName());

    @Autowired
    KycService kycService;

    private NaturalPerson naturalPerson;

    private Converter converter;

    public KycController() {

        converter = new Converter();

        naturalPerson = new NaturalPerson();
        naturalPerson.setDate(new Date());
        naturalPerson.setDocumentType(IKyc.DocumentType.KYCNP.toString());
    }

    @GetMapping("/naturalPerson")
    public String naturalPerson(@ModelAttribute("naturalPerson") NaturalPerson naturalPerson, Model model) {

        naturalPerson = new NaturalPerson();
        naturalPerson.setDate(new Date());
        naturalPerson.setDocumentType(IKyc.DocumentType.KYCNP.toString());
        
        model.addAttribute("naturalPerson", naturalPerson);

        return "kyc/naturalPerson";
    }

    @PostMapping(value = "/naturalPerson", params = {"addOther"})
    public String addRow(@Validated(value = OtherValidator.class) @ModelAttribute("naturalPerson") NaturalPerson naturalPerson,
            BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            LOG.log(Level.WARNING, "There are validation errors");
            List<ObjectError> allErrors = bindingResult.getAllErrors();
            for (ObjectError oe : allErrors) {
                LOG.log(Level.INFO, "Error =>  {0}", oe);
            }

            return "kyc/naturalPerson";
        }

        this.getNaturalPerson().getSourceIncomeList().add(naturalPerson.getOther());

        this.getNaturalPerson().setOther(null);
        this.getNaturalPerson().setIncomeSourceType("default");

        return "kyc/naturalPerson";
    }

    @PostMapping(value = "/naturalPerson", params = {"sourceIncome"})
    public String removeRow(final HttpServletRequest req, Model model) {

        final Integer sourceIncomeId = Integer.valueOf(req.getParameter("sourceIncome"));

        LOG.log(Level.INFO, "in removeRow {0}", sourceIncomeId);

        SourceIncome element = getElemetByIndex(this.getNaturalPerson().getSourceIncomeList(), sourceIncomeId);
        this.getNaturalPerson().getSourceIncomeList().remove(element);

//        int count = 0;
//        for (SourceIncome element : SourceIncome element) {
//            if (sourceIncomeId == count) {
//                LOG.log(Level.INFO, "item to remove {0}", element);
//                this.getNaturalPerson().getSourceIncomeList().remove(element);
//            }
//
//            count++;
//        }
        return "kyc/naturalPerson";
    }

    @PostMapping("/naturalPerson")
    public String addNaturalPerson(@Valid @ModelAttribute("naturalPerson") NaturalPerson naturalPerson,
            BindingResult bindingResult, Model model, final RedirectAttributes redirectAttributes) throws ConversionException, Exception {

        LOG.log(Level.INFO, "in add addNaturalPerson {0}", naturalPerson.toString());

        if (bindingResult.hasErrors()) {
            LOG.log(Level.WARNING, "There are validation errors");
            List<ObjectError> allErrors = bindingResult.getAllErrors();
            for (ObjectError oe : allErrors) {
                LOG.log(Level.INFO, "Error =>  {0}", oe);
            }

            return "kyc/naturalPerson";
        }

        String xml = converter.toXml(naturalPerson);

        ValidateResponse validateResponse = kycService.validateDocument(naturalPerson.getDocumentType(), xml);

        LOG.log(Level.INFO, "xml representation {0}", validateResponse);

        if (validateResponse.getStatus().equals("VALID")) {

            SignatureParameters signatureParameters = new SignatureParameters();
            Document document = new Document();

            document.setEncodedDocument(Utils.toBase64(validateResponse.getDocument().getBytes()));
            document.setMimeType(MimeType.XML);
            document.setName(naturalPerson.getIdentificationNumber() + "." + MimeType.getExtension(MimeType.XML));

            BLevelParameters bLevelParameters = new BLevelParameters();

            TimestampParameters timestampParameters = new TimestampParameters();

            bLevelParameters.setSigningDate(new Date());
            timestampParameters.setDigestAlgorithm(DigestAlgorithm.SHA512);

            signatureParameters.setSignWithExpiredCertificate(true);
            signatureParameters.setSignatureLevel(SignatureLevel.XAdES_XL);
            signatureParameters.setSignaturePackaging(SignaturePackaging.ENVELOPING);
            signatureParameters.setDigestAlgorithm(DigestAlgorithm.SHA512);
            signatureParameters.setBLevelParams(bLevelParameters);

            signatureParameters.setContentTimestampParameters(timestampParameters);
            signatureParameters.setSignatureTimestampParameters(timestampParameters);
            signatureParameters.setArchiveTimestampParameters(timestampParameters);
            signatureParameters.setDocument(document);

            redirectAttributes.addFlashAttribute("signatureParameters", signatureParameters);

            return "redirect:/dss/signatureProcess";
        }

        return "kyc/naturalPerson";
    }

    @ModelAttribute("documentTypeList")
    public List<IKyc.DocumentType> documentTypeList() throws Exception {

        return kycService.documentTypeResponse().getDocumentTypeList();

    }

    @ModelAttribute("identificationTypeList")
    public List<IKyc.IdentificacionType> identificationTypeList() throws Exception {

        List<IKyc.IdentificacionType> identificationTypeList = kycService.identificationTypeResponse().getIdentificationTypeList();
        identificationTypeList.remove(IKyc.IdentificacionType.LegalPerson);

        return identificationTypeList;

    }

    @ModelAttribute("genderTypeList")
    public List<INaturalPerson.GenderType> genderTypeList() throws Exception {

//        List<String> list = new ArrayList<>();
//        list.add("Male");
//        list.add("Female");
//        
//        return list;
        return kycService.genderTypeResponse().getGenderTypeList();

    }

    @ModelAttribute("maritalStatusList")
    public List<INaturalPerson.MaritalStatusType> genderMaritalStatus() throws Exception {

        return kycService.maritalStatusResponse().getMaritalStatusList();

    }

    @ModelAttribute("nationalityList")
    public List<Nationality> nationality() throws Exception {
        List<Nationality> list = new ArrayList<>();
        Nationality nationality = new Nationality();
        nationality.setIso("CRI");

        list.add(nationality);

        return list;

    }

    @ModelAttribute("sourceIncomeList")
    public List<INaturalPerson.SourceIncomeType> sourceIncome() throws Exception {

//        List<String> list = new ArrayList<>();
//        list.add("Other");
//        
//        return list;
        return kycService.sourceIncomeResponse().getSourceIncomeList();

    }

    @ModelAttribute("provinceList")
    public List<KycProvince> provinceList() throws Exception {

//        List<String> list = new ArrayList<>();
//        list.add("Other");
//        
//        return list;
        return kycService.provinceListResponse();

    }

    @ModelAttribute("naturalPerson")
    public NaturalPerson getNaturalPerson() {
        LOG.log(Level.INFO, "list size {0}", naturalPerson.getSourceIncomeList().size());
        return this.naturalPerson;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("dd-MM-yyyy"), true));
    }

    private static <T> T getElemetByIndex(Set<T> set, int n) {
        if (null != set && n >= 0 && n < set.size()) {
            int count = 0;
            for (T element : set) {
                if (n == count) {
                    return element;
                }
                count++;
            }
        }
        return null;
    }
}
