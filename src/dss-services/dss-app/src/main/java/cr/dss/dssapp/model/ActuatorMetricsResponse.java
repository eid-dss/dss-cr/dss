/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.dssapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author esolis
 */
public class ActuatorMetricsResponse implements Serializable {

    private String mem;
    private String memFree;
    private String processors;
    private Long uptime;
    private String uptimeFormated;
    private Double systemloadAverage;

    public ActuatorMetricsResponse() {
    }

    public String getMem() {
        return mem;
    }

    public void setMem(String mem) {
        this.mem = mem;
    }

    @JsonProperty("mem.free")
    public String getMemFree() {
        return memFree;
    }

    public void setMemFree(String memFree) {
        this.memFree = memFree;
    }

    public String getProcessors() {
        return processors;
    }

    public void setProcessors(String processors) {
        this.processors = processors;
    }

    public Long getUptime() {
        return uptime;
    }

    public void setUptime(Long uptime) {
        this.uptime = uptime;
        this.uptimeFormated = calculateTime(uptime);
    }

    @JsonProperty("systemload.average")
    public Double getSystemloadAverage() {
        return systemloadAverage;
    }

    public void setSystemloadAverage(Double systemloadAverage) {
        this.systemloadAverage = systemloadAverage;
    }

    public String getUptimeFormated() {
        return uptimeFormated;
    }

    public static String calculateTime(Long timeIn) {
        int day = (int) TimeUnit.MILLISECONDS.toDays(timeIn);
        long hours = TimeUnit.MILLISECONDS.toHours(timeIn) - (day * 24);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(timeIn) - (TimeUnit.MILLISECONDS.toHours(timeIn) * 60);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(timeIn) - (TimeUnit.MILLISECONDS.toMinutes(timeIn) * 60);

        String time = String.valueOf(day) + " Days " + String.valueOf(hours) + " Hours "
                + String.valueOf(minutes) + " Minutes " + String.valueOf(seconds) + " Seconds.";

        return time;
    }

    @Override
    public String toString() {
        return "ActuatorMetricsResponse{" + "mem=" + mem + ", memFree=" + memFree + ", processors=" + processors + ", uptime=" + uptime + ", systemloadAverage=" + systemloadAverage + '}';
    }

}
