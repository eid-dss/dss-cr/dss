/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.dssapp.repository;

import cr.dss.model.Event;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author esolis
 */
@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

    @Override
    List<Event> findAll();

}
