/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.dssapp.controller;

import cr.dss.dssapp.service.ActuatorService;
import cr.dss.dssapp.model.ActuatorInfoResponse;
import cr.dss.dssapp.model.ActuatorMetricsResponse;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author esolis
 */
@Controller
@RequestMapping("/")
public class IndexController {

    private static final Logger LOG = Logger.getLogger(IndexController.class.getName());

    @Autowired
    ActuatorService actuatorService;

    @GetMapping
    public String showIndex(Map<String, Object> model) {

        return "index";
    }

    @GetMapping("/dashboard")
    public String showDashboard(Map<String, Object> model) {

        ActuatorInfoResponse actuatorInfoResponse = actuatorService.info();
        ActuatorMetricsResponse actuatorMetricsResponse = actuatorService.metrics();

        LOG.log(Level.INFO, "Time test {0}", ActuatorMetricsResponse.calculateTime(actuatorMetricsResponse.getUptime()));
        
        model.put("actuatorInfoResponse", actuatorInfoResponse);
        model.put("actuatorMetricsResponse", actuatorMetricsResponse);

        return "dashboard";
    }
    
    @ExceptionHandler
    void handleIllegalArgumentException(
                      IllegalArgumentException e,
                      HttpServletResponse response) throws IOException {
 
        response.sendError(HttpStatus.BAD_REQUEST.value());
 
    }

}
