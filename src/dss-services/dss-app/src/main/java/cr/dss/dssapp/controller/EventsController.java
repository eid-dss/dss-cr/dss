/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.dssapp.controller;

import cr.dss.dssapp.model.ActuatorInfoResponse;
import cr.dss.dssapp.model.ActuatorMetricsResponse;
import cr.dss.dssapp.repository.EventRepository;
import cr.dss.model.Event;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author esolis
 */
@Controller
@RequestMapping()
public class EventsController {

    private static final Logger LOG = Logger.getLogger(EventsController.class.getName());

    @Autowired
    private EventRepository eventRepository;

    @GetMapping("/events")
    public String getEvents(Map<String, Object> model) {

        List<Event> eventList = eventRepository.findAll();

        LOG.log(Level.INFO, "List count {0}", eventList.size());

        model.put("eventList", eventList);

        return "events";
    }
    
    @GetMapping("event/{id}")
    public String getEventById(@PathVariable Long id, Map<String, Object> model) {

        Event event = eventRepository.findOne(id);

        LOG.log(Level.INFO, "Event by Id", event);

        model.put("event", event);

        return "event";
    }
}
