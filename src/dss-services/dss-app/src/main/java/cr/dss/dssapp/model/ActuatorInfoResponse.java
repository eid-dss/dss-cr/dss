/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.dssapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author esolis
 */
public class ActuatorInfoResponse implements Serializable {

    private App app;

    public ActuatorInfoResponse() {
    }

    public ActuatorInfoResponse(App app) {
        this.app = app;
    }

    @JsonProperty("app")
    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    @Override
    public String toString() {
        return "ActuatorInfoResponse{" + "app=" + app + '}';
    }

}
