/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.dssapp.service;

import cr.dss.dssapp.model.ActuatorInfoResponse;
import cr.dss.dssapp.model.ActuatorMetricsResponse;
import cr.dss.dssapp.properties.ApplicationProperties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author esolis
 */
@Component
public class ActuatorService {

    private static final Logger LOG = Logger.getLogger(ActuatorService.class.getName());

    private static final String APP_URL = "http://127.0.0.1:8091/";

    @Autowired
    private ApplicationProperties applicationProperties;

    public ActuatorInfoResponse info() throws HttpClientErrorException {

        LOG.log(Level.INFO, "*** Info ***");

        String url = applicationProperties.getDssServicesUrl() + "info";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ActuatorInfoResponse actuatorInfoResponse = restTemplate.getForObject(url, ActuatorInfoResponse.class);
            LOG.log(Level.INFO, "Response from info {0}", actuatorInfoResponse.toString());
            
            return actuatorInfoResponse;

        } catch (HttpClientErrorException e) {
            throw e;
        }

    }

    public ActuatorMetricsResponse metrics() throws HttpClientErrorException {

        LOG.log(Level.INFO, "*** Metrics ***");

        String url = APP_URL + "metrics";

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        try {
            ActuatorMetricsResponse actuatorMetricsResponse = restTemplate.getForObject(url, ActuatorMetricsResponse.class);

            LOG.log(Level.INFO, "Response from metrics {0}", actuatorMetricsResponse);
            
            return actuatorMetricsResponse;

        } catch (HttpClientErrorException e) {
            throw e;
        }

    }

}
