/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.dssapp.properties;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author esolis
 */
@Configuration
@ConfigurationProperties
@Validated
public class ApplicationProperties {

    @NotBlank
    @Value("${dss.services.url}")
    private String dssServicesUrl;

    public String getDssServicesUrl() {
        return dssServicesUrl;
    }

    public void setDssServicesUrl(String dssServicesUrl) {
        this.dssServicesUrl = dssServicesUrl;
    }

}
