/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.config;

import cr.dss.audit.JpaServiceAuditEventRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author esolis
 */
@Configuration
public class DataBaseBeanConfig {

    @Bean
    public JpaServiceAuditEventRepository serviceAuditEventRepository() throws Exception {
        return new JpaServiceAuditEventRepository();
    }

}
