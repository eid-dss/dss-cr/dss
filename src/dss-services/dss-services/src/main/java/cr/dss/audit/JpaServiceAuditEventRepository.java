/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.audit;

import cr.dss.audit.domain.Event;
import cr.dss.audit.repository.EventRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author esolis
 */
public class JpaServiceAuditEventRepository implements ServiceAuditEventRepository {

    private static final Logger LOG = Logger.getLogger(JpaServiceAuditEventRepository.class.getName());

    @Autowired
    private EventRepository eventRepository;

    @Override
    public void add(Event event) {
        LOG.log(Level.INFO, "Adding into database {0}", event.toString());
        this.eventRepository.save(event);
    }

    @Override
    public List<Event> findAll() {
        return this.eventRepository.findAll();
    }

}
