/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.utils;

import eu.europa.esig.dss.DSSDocument;
import eu.europa.esig.dss.DSSException;
import eu.europa.esig.dss.InMemoryDocument;
import eu.europa.esig.dss.RemoteCertificate;
import eu.europa.esig.dss.utils.Utils;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author esolis
 */
public class DssUtils {

    private static final Logger LOG = Logger.getLogger(DssUtils.class.getName());

    private DssUtils() {
    }

    public static String getRemoteAddr(final HttpServletRequest request) {
        final String ipFromHeader = request.getHeader("X-FORWARDED-FOR");
        if (ipFromHeader != null && ipFromHeader.length() > 0) {
            LOG.log(Level.INFO, "ip from proxy - X-FORWARDED-FOR : {0}", ipFromHeader);
            return ipFromHeader;
        }
        return request.getRemoteAddr();
    }

    public static String randomStringUUID() {
        LOG.log(Level.INFO, "Generating RandomStringUUID");
        UUID uuid = UUID.randomUUID();

        return uuid.toString();
    }

    public static DSSDocument toDSSDocument(String file) {
        LOG.log(Level.INFO, "File: {0}", file);
        if ((file != null) && !file.isEmpty()) {
            DSSDocument document = new InMemoryDocument(file.getBytes());
            LOG.log(Level.INFO, "DSSDocument: {0}", document);
            return document;
        }

        LOG.log(Level.SEVERE, "Cannot read  file ");
        return null;

    }

    public static RemoteCertificate toRemoteCertificate(String encodedCertificate) throws DSSException {
        RemoteCertificate remoteCertificate = new RemoteCertificate();
        try {
            LOG.info("encodedCertificate: " + encodedCertificate);
            remoteCertificate.setEncodedCertificate(Utils.fromBase64(encodedCertificate));

        } catch (Exception e) {

            throw new DSSException("Cannot read  Encoded Certificate", e);
        }
        return remoteCertificate;
    }

    public static List<RemoteCertificate> toCertificateChain(List<String> certificateChain) throws DSSException {
        List<RemoteCertificate> remoteCertificateList = new ArrayList<>();
        RemoteCertificate remoteCertificate = new RemoteCertificate();
        try {
            for (String certificate : certificateChain) {

                remoteCertificate.setEncodedCertificate(Utils.fromBase64(certificate));
                remoteCertificateList.add(remoteCertificate);
            }

        } catch (Exception e) {

            throw new DSSException("Cannot read  Encoded Certificate");
        }
        return remoteCertificateList;
    }

}
