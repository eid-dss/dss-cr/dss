/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.controller;

import cr.dss.model.DataToSignRequest;
import cr.dss.model.Response;
import cr.dss.model.SignDocumentRequest;
import cr.dss.model.TestRequest;

/**
 *
 * @author esolis
 */
public interface IRemoteDocumentSignatureService {

    Response test(TestRequest testRequest);

    Response dataToSign(DataToSignRequest dataToSignRequest);

    Response signDocument(SignDocumentRequest signDocumentRequest);

}
