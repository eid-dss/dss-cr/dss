/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.model;

import java.util.Objects;

/**
 *
 * @author esolis
 */
public class Feedback {

    private final FeedbackStatus feedbackStatus;
    private final String stacktrace;

    public Feedback(FeedbackStatus feedbackStatus, String stacktrace) {
        this.feedbackStatus = feedbackStatus;
        this.stacktrace = stacktrace;
    }

    public FeedbackStatus getFeedbackStatus() {
        return feedbackStatus;
    }

    public String getStacktrace() {
        return stacktrace;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.feedbackStatus);
        hash = 41 * hash + Objects.hashCode(this.stacktrace);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Feedback other = (Feedback) obj;
        if (this.feedbackStatus != other.feedbackStatus) {
            return false;
        }
        if (!Objects.equals(this.stacktrace, other.stacktrace)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Feedback{" + "feedbackStatus=" + feedbackStatus + ", stacktrace=" + stacktrace + '}';
    }
    
}
