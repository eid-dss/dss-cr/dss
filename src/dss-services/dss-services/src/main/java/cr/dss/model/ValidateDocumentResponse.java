/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.model;

import eu.europa.esig.dss.jaxb.detailedreport.DetailedReport;
import eu.europa.esig.dss.jaxb.diagnostic.DiagnosticData;
import eu.europa.esig.dss.jaxb.simplereport.SimpleReport;
import eu.europa.esig.dss.validation.reports.Reports;

import java.io.Serializable;

/**
 *
 * @author esolis
 */

public class ValidateDocumentResponse implements Serializable {

    private DiagnosticData diagnosticData;
    private SimpleReport simpleReport;
    private DetailedReport detailedReport;

    public ValidateDocumentResponse(DiagnosticData diagnosticData, SimpleReport simpleReport, DetailedReport detailedReport) {
        this.diagnosticData = diagnosticData;
        this.detailedReport = detailedReport;
        this.simpleReport = simpleReport;
    }
    
    public ValidateDocumentResponse(Reports reports) {
        this.diagnosticData = reports.getDiagnosticDataJaxb();
        this.detailedReport = reports.getDetailedReportJaxb();
        this.simpleReport = reports.getSimpleReportJaxb();
    }

    public DiagnosticData getDiagnosticData() {
        return diagnosticData;
    }

    public void setDiagnosticData(DiagnosticData diagnosticData) {
        this.diagnosticData = diagnosticData;
    }

    public SimpleReport getSimpleReport() {
        return simpleReport;
    }

    public void setSimpleReport(SimpleReport simpleReport) {
        this.simpleReport = simpleReport;
    }

    public eu.europa.esig.dss.jaxb.detailedreport.DetailedReport getDetailedReport() {
        return detailedReport;
    }

    public void setDetailedReport(eu.europa.esig.dss.jaxb.detailedreport.DetailedReport detailedReport) {
        this.detailedReport = detailedReport;
    }

    @Override
    public String toString() {
        return "ValidateDocumentResponse{" + "simpleReport=" + simpleReport + ", detailedReport=" + detailedReport + '}';
    }

}
