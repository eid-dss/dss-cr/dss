/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 *
 * @author esolis
 */
public class ConstraintUtils {

    public static <T> Set<ConstraintViolation<T>> validateBean(T clazz) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> violations = validator.validate(clazz);
        return violations;
    }

    public static <T> List<String[]> getInvalidProperties(Set<ConstraintViolation<T>> cv) {

        List<String[]> invalidFields = new ArrayList<String[]>();
        Iterator<ConstraintViolation<T>> iterator = cv.iterator();
        while (iterator.hasNext()) {
            ConstraintViolation<T> i = iterator.next();
            String property = i.getPropertyPath().toString();
            String message = i.getMessage();
            invalidFields.add(new String[]{property, message});
        }

        return invalidFields;
    }
}
