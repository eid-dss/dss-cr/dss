/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.model;

/**
 *
 * @author esolis
 */
public class DataToSignResponse{

    private String dataToSign;

    public DataToSignResponse() {
    }


    public String getDataToSign() {
        return dataToSign;
    }

    public void setDataToSign(String dataToSign) {
        this.dataToSign = dataToSign;
    }

    @Override
    public String toString() {
        return "DataToSignResponse{" + "dataToSign=" + dataToSign + '}';
    }
    
    
}
