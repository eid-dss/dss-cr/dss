/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.rest;

import cr.dss.model.Response;
import cr.dss.model.ValidateDocumentRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author esolis
 */
@CrossOrigin
@RequestMapping(value = "/rest/dss")
public interface DssRestValidation {
    
    @RequestMapping(value = "/validate-document", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Response> validateDocument(ValidateDocumentRequest validateDocumentRequest);

}