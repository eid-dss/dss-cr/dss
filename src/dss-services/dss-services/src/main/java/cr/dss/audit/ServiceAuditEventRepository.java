/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.audit;

import cr.dss.audit.domain.Event;
import java.util.List;

/**
 *
 * @author esolis
 */
public interface ServiceAuditEventRepository {

    /**
     * Log an event.
     *
     * @param event the audit event to log
     */
    void add(Event event);
    
    List<Event> findAll();

}
