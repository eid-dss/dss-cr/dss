/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.rest;

import cr.dss.model.Response;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author esolis
 */
@CrossOrigin
@RequestMapping(value = "/rest/dss")
public interface DssRestTsl {
    
    @RequestMapping(value = "/tsl-info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getSummary();
    
    @RequestMapping(value = "/tsl-info/{country:[a-z][a-z]}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getByCountry(String countryCode);
}
