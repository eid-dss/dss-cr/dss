/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.job;

import eu.europa.esig.dss.tsl.service.TSLValidationJob;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author esolis
 */
@Service
public class TSLLoaderJob {

    @Value("${cron.tl.loader.enable}")
    private boolean enable;

    @Autowired
    private TSLValidationJob job;

    @PostConstruct
    public void init() {
        job.initRepository();
    }
    
    @Scheduled(initialDelayString = "${cron.initial.delay.tl.loader}", fixedDelayString = "${cron.delay.tl.loader}")
    public void refresh() {
        if (enable) {
            job.refresh();
        }
    }
}
