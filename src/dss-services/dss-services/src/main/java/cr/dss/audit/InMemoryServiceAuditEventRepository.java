/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.audit;

import cr.dss.audit.ServiceAuditEventRepository;
import cr.dss.audit.domain.Event;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.util.Assert;

/**
 *
 * @author esolis
 */
public class InMemoryServiceAuditEventRepository implements ServiceAuditEventRepository {

    private static final Logger LOG = Logger.getLogger(InMemoryServiceAuditEventRepository.class.getName());

    private static final int DEFAULT_CAPACITY = 1000;

    private final Object monitor = new Object();

    /**
     * Circular buffer of the event with tail pointing to the last element.
     */
    private Event[] events;

    private volatile int tail = -1;

    public InMemoryServiceAuditEventRepository() {
        this(DEFAULT_CAPACITY);
    }

    public InMemoryServiceAuditEventRepository(int capacity) {
        this.events = new Event[capacity];
    }

    /**
     * Set the capacity of this event repository.
     *
     * @param capacity the capacity
     */
    public void setCapacity(int capacity) {
        synchronized (this.monitor) {
            this.events = new Event[capacity];
        }
    }

    @Override
    public void add(Event event) {
        Assert.notNull(event, "AuditEvent must not be null");
        synchronized (this.monitor) {
            this.tail = (this.tail + 1) % this.events.length;
            this.events[this.tail] = event;
            
            LOG.log(Level.INFO, event.toString());
        }
    }

    @Override
    public List<Event> findAll() {
        return find(null, null);
    }

    public List<Event> find(Date after, String type) {
        LinkedList<Event> events = new LinkedList<>();
        synchronized (this.monitor) {
            for (int i = 0; i < this.events.length; i++) {
                Event event = resolveTailEvent(i);
                if (event != null && isMatch(after, type, event)) {
                    events.addFirst(event);
                }
            }
        }
        return events;
    }

    private boolean isMatch(Date after, String type, Event event) {
        boolean match = true;
        match = match && (after == null || event.getCreationTime().compareTo(after) >= 0);
        match = match && (type == null || event.getEvent().equals(type));
        return match;
    }

    private Event resolveTailEvent(int offset) {
        int index = ((this.tail + this.events.length - offset) % this.events.length);
        return this.events[index];
    }

}
