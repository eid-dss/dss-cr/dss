/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.config;

import cr.dss.audit.InMemoryServiceAuditEventRepository;
import cr.dss.audit.ServiceAuditEventRepository;
import java.util.concurrent.Executor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 *
 * @author esolis
 */
@Configuration
@EnableAsync
public class TaskExecutorConfig extends AsyncConfigurerSupport {

    @Override
    public Executor getAsyncExecutor() {
        return new SimpleAsyncTaskExecutor();
    }
    
//    @ConditionalOnMissingBean(ServiceAuditEventRepository.class)
//    protected static class ServiceAuditEventRepositoryConfiguration {
//
//        @Bean
//        public InMemoryServiceAuditEventRepository serviceAuditEventRepository() throws Exception {
//            return new InMemoryServiceAuditEventRepository();
//        }
//
//    }

}
