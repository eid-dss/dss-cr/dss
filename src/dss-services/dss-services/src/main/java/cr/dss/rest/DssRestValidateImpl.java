/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cr.dss.audit.domain.Event;
import cr.dss.audit.domain.EventDetail;
import cr.dss.audit.domain.IEvent;
import cr.dss.model.DataToSignRequest;
import cr.dss.validation.DocumentValidationServiceImpl;
import cr.dss.model.Document;
import cr.dss.model.Feedback;
import cr.dss.model.FeedbackStatus;
import cr.dss.model.Response;
import cr.dss.model.ValidateDocumentRequest;
import cr.dss.model.ValidateDocumentResponse;
import cr.dss.utils.ConstraintUtils;
import cr.dss.utils.DssUtils;
import eu.europa.esig.dss.RemoteDocument;
import eu.europa.esig.dss.utils.Utils;
import eu.europa.esig.dss.validation.reports.Reports;
import eu.europa.esig.dss.validation.reports.dto.ReportsDTO;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author esolis
 */
@RestController
public class DssRestValidateImpl implements DssRestValidation {

    private static final Logger LOG = Logger.getLogger(DssRestValidateImpl.class.getName());

    private static final String ERROR_MESSAGE = "An exception was thrown during the operation.";

    Feedback feedback;
    Response response;
    ResponseEntity<Response> responseEntity;

    EventDetail eventDetail;
    Event event;

    private final Gson gson = new GsonBuilder().create();

    @Autowired
    private DocumentValidationServiceImpl documentValidationServiceImpl;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Override
    public ResponseEntity<Response> validateDocument(@RequestBody ValidateDocumentRequest validateDocumentRequest) {
        LOG.log(Level.INFO, "Start DssResource | validateDocument");

        LOG.log(Level.INFO, "Request: {0}", validateDocumentRequest.toString());

        event = new Event();
        eventDetail = new EventDetail();

        event.setEvent(IEvent.EventType.ValidateDocument);
        event.setIdTransaction(DssUtils.randomStringUUID());

        eventDetail.setRequest(gson.toJson(validateDocumentRequest));

        Set<ConstraintViolation<ValidateDocumentRequest>> constraintViolations = ConstraintUtils.validateBean(validateDocumentRequest);

        if (constraintViolations.size() > 0) {

            StringBuilder mensaje = new StringBuilder();

            List<String[]> invalidPropertiesList = ConstraintUtils.getInvalidProperties(constraintViolations);

            for (String[] invalidProperties : invalidPropertiesList) {
                mensaje.append(Arrays.toString(invalidProperties));
            }

            throw new ValidationException(mensaje.toString());

        }

        Document originalDocument = validateDocumentRequest.getOriginalDocument();
        Document signedDocument = validateDocumentRequest.getSignedDocument();

        byte[] originalDocumentByte = validateDocumentRequest.isOriginalDocument() ? Utils.fromBase64(originalDocument.getEncodedDocument()) : null;
        byte[] signedDocumentByte = Utils.fromBase64(signedDocument.getEncodedDocument());

        RemoteDocument remoteOriginalDocument = validateDocumentRequest.isOriginalDocument() ? new RemoteDocument(originalDocumentByte, originalDocument.getMimeType(), originalDocument.getName()) : new RemoteDocument();
        RemoteDocument remoteSignedDocument = new RemoteDocument(signedDocumentByte, signedDocument.getMimeType(), signedDocument.getName());

        try {

            Reports reports = documentValidationServiceImpl.validateDocument(remoteSignedDocument, remoteOriginalDocument, validateDocumentRequest.getPolicy());

            ValidateDocumentResponse validateDocumentResponse = new ValidateDocumentResponse(reports);

            response = new Response<>(validateDocumentResponse);

            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
            
            eventDetail.setResponse(gson.toJson(validateDocumentResponse));

            event.setStatus(IEvent.StatusType.Success);

        } catch (ValidationException ex) {
            LOG.log(Level.SEVERE, null, ex);

            feedback = new Feedback(FeedbackStatus.EXCEPTION, ex.getLocalizedMessage());
            response = new Response<>(ex, "exception", ERROR_MESSAGE, feedback);

            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

            eventDetail.setResponse(gson.toJson(feedback));

            event.setStatus(IEvent.StatusType.Fail);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);

            feedback = new Feedback(FeedbackStatus.EXCEPTION, ex.getLocalizedMessage());
            response = new Response<>(ex, "exception", ERROR_MESSAGE, feedback);

            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

            eventDetail.setResponse(gson.toJson(feedback));

            event.setStatus(IEvent.StatusType.Fail);

        }

        event.setCreationTime(new Date());
        event.setEventDetail(eventDetail);

        publisher.publishEvent(event);

        LOG.log(Level.INFO, "Response: {0}", responseEntity.toString());

        LOG.log(Level.INFO, "End DssResource | validateDocument");

        return responseEntity;
    }

}
