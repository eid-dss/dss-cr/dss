/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.audit.domain;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author esolis
 */
public interface IEvent {

    enum EventType {

        DataToSign("DATATOSIGN", "Data to Sign"), SignDocument("SIGNDOCUMENT", "Sign Document"),
        ValidateDocument("VALIDATEDOCUMENT", "Validate Document"), Test("TEST", "Test");

        private final String value;
        private final String label;

        static Map<String, EventType> map = new HashMap<>();

        static {
            for (EventType e : EventType.values()) {
                map.put(e.value, e);
            }
        }

        private EventType(String value, String label) {
            this.value = value;
            this.label = label;
        }

        public String getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }

        public static EventType fromString(String value) {
            return map.get(value);
        }

    }

    enum StatusType {

        Success("S", "Success"), Fail("F", "Fail");

        private final String value;
        private final String label;

        static Map<String, StatusType> map = new HashMap<>();

        static {
            for (StatusType e : StatusType.values()) {
                map.put(e.value, e);
            }
        }

        private StatusType(String value, String label) {
            this.value = value;
            this.label = label;
        }

        public String getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }

        public static StatusType fromString(String value) {
            return map.get(value);
        }

    }
}
