/**
 * DSS - Digital Signature Services Copyright (C) 2015 European Commission,
 * provided under the CEF programme
 *
 * This file is part of the "DSS - Digital Signature Services" project.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
package cr.dss.model;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * Parameters for a Signature creation/extension
 *
 */
@SuppressWarnings("serial")
public abstract class ApplicationParameters implements Serializable {

    @NotNull
    private String idTransaction;

    private String businessApplication;

    public String getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(String idTransaction) {
        this.idTransaction = idTransaction;
    }

    public String getBusinessApplication() {
        return businessApplication;
    }

    public void setBusinessApplication(String businessApplication) {
        this.businessApplication = businessApplication;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.idTransaction);
        hash = 89 * hash + Objects.hashCode(this.businessApplication);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApplicationParameters other = (ApplicationParameters) obj;
        if (!Objects.equals(this.idTransaction, other.idTransaction)) {
            return false;
        }
        if (!Objects.equals(this.businessApplication, other.businessApplication)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ApplicationParameters{" + "idTransaction=" + idTransaction + ", businessApplication=" + businessApplication + '}';
    }

}
