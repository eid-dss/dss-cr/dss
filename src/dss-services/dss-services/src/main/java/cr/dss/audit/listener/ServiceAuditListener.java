/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.audit.listener;

import cr.dss.audit.ServiceAuditEventRepository;
import cr.dss.audit.domain.Event;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 *
 * @author esolis
 */
@Component
public class ServiceAuditListener {

    private static final Log logger = LogFactory.getLog(ServiceAuditListener.class);

    private final ServiceAuditEventRepository serviceAuditEventRepository;

    /**
     * Authentication success event type.
     */
    public static final String TEST_SUCCESS = "TEST_SUCCESS";

    public ServiceAuditListener(ServiceAuditEventRepository serviceAuditEventRepository) {
        this.serviceAuditEventRepository = serviceAuditEventRepository;
    }

    @Async
    @EventListener
    protected void onAuditEvent(Event event) {
        if (logger.isDebugEnabled()) {
            logger.debug(event);
        }
        this.serviceAuditEventRepository.add(event);
    }
}
