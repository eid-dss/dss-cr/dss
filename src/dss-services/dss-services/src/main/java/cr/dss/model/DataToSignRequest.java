/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.model;

import javax.validation.constraints.NotNull;

import java.io.Serializable;

import java.util.List;

/**
 *
 * @author esolis
 */
public class DataToSignRequest extends AbstractSerializableSignatureParameters implements Serializable {

    @NotNull
    private String signingCertificate;
    
    private List<String> certificateChain;
    
    @NotNull
    private Document document;

    public DataToSignRequest() {
    }

    public DataToSignRequest(String signingCertificate, List<String> certificateChain, Document document) {
        this.signingCertificate = signingCertificate;
        this.certificateChain = certificateChain;

    }

    public String getSigningCertificate() {
        return signingCertificate;
    }

    public void setSigningCertificate(String signingCertificate) {
        this.signingCertificate = signingCertificate;
    }

    public List<String> getCertificateChain() {
        return certificateChain;
    }

    public void setCertificateChain(List<String> certificateChain) {
        this.certificateChain = certificateChain;
    }

    public Document getDocument() {
        return document;
    }

    public void setEncodedDocument(Document document) {
        this.document = document;
    }

    @Override
    public String toString() {
        return "DataToSignRequest{" + super.toString() + "signingCertificate=" + signingCertificate + ", certificateChain=" + certificateChain + ", document=" + document + '}';
    }

    
    

}
