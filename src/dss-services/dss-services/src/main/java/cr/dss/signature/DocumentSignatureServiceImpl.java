/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.signature;

//import cr.dss.controller.tsl.TslServiceImpl;
import eu.europa.esig.dss.ASiCContainerType;
import eu.europa.esig.dss.AbstractSignatureParameters;
import eu.europa.esig.dss.DSSDocument;
import eu.europa.esig.dss.DSSException;
import eu.europa.esig.dss.RemoteDocument;
import eu.europa.esig.dss.RemoteSignatureParameters;
import eu.europa.esig.dss.SignatureForm;
import eu.europa.esig.dss.SignatureLevel;
import eu.europa.esig.dss.SignatureValue;
import eu.europa.esig.dss.ToBeSigned;
import eu.europa.esig.dss.asic.ASiCWithCAdESSignatureParameters;
import eu.europa.esig.dss.asic.ASiCWithXAdESSignatureParameters;
import eu.europa.esig.dss.cades.signature.CAdESService;
import eu.europa.esig.dss.pades.PAdESSignatureParameters;
import eu.europa.esig.dss.signature.DocumentSignatureService;
import eu.europa.esig.dss.signature.RemoteDocumentSignatureService;
import eu.europa.esig.dss.xades.signature.XAdESService;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author esolis
 */
@Service
public class DocumentSignatureServiceImpl extends AbstractRemoteSignatureServiceImpl
        implements RemoteDocumentSignatureService<RemoteDocument, RemoteSignatureParameters> {

    private static final Logger LOG = Logger.getLogger(DocumentSignatureServiceImpl.class.getName());

    @Autowired
    private XAdESService xadesService;

    @Autowired
    private CAdESService cadesService;

    private DocumentSignatureService<PAdESSignatureParameters> padesService;

    private DocumentSignatureService<ASiCWithXAdESSignatureParameters> asicWithXAdESService;

    private DocumentSignatureService<ASiCWithCAdESSignatureParameters> asicWithCAdESService;

    @SuppressWarnings("rawtypes")
    private DocumentSignatureService getServiceForSignature(RemoteSignatureParameters parameters) {
        ASiCContainerType asicContainerType = parameters.getAsicContainerType();
        SignatureLevel signatureLevel = parameters.getSignatureLevel();
        SignatureForm signatureForm = signatureLevel.getSignatureForm();
        if (asicContainerType != null) {
            switch (signatureForm) {
                case XAdES:
                    return asicWithXAdESService;
                case CAdES:
                    return asicWithCAdESService;
                default:
                    throw new DSSException("Unrecognized format (XAdES or CAdES are allowed with ASiC) : " + signatureForm);
            }
        } else {
            switch (signatureForm) {
                case XAdES:
                    return xadesService;
                case CAdES:
                    return cadesService;
                case PAdES:
                    return padesService;
                default:
                    throw new DSSException("Unrecognized format " + signatureLevel);
            }
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public ToBeSigned getDataToSign(RemoteDocument remoteDocument, RemoteSignatureParameters remoteParameters) throws DSSException {
        LOG.info("GetDataToSign in process...");
        AbstractSignatureParameters parameters = createParameters(remoteParameters);
        DocumentSignatureService service = getServiceForSignature(remoteParameters);
        DSSDocument dssDocument = createDSSDocument(remoteDocument);
        ToBeSigned dataToSign = service.getDataToSign(dssDocument, parameters);
        LOG.info("GetDataToSign is finished");
        return dataToSign;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public DSSDocument signDocument(RemoteDocument remoteDocument, RemoteSignatureParameters remoteParameters, SignatureValue signatureValue)
            throws DSSException {
        LOG.info("SignDocument in process...");
        AbstractSignatureParameters parameters = createParameters(remoteParameters);
        DocumentSignatureService service = getServiceForSignature(remoteParameters);
        DSSDocument dssDocument = createDSSDocument(remoteDocument);
        DSSDocument signDocument = service.signDocument(dssDocument, parameters, signatureValue);
        LOG.info("SignDocument is finished");
        return signDocument;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public DSSDocument extendDocument(RemoteDocument remoteDocument, RemoteSignatureParameters remoteParameters) throws DSSException {
        LOG.info("ExtendDocument in process...");
        AbstractSignatureParameters parameters = createParameters(remoteParameters);
        DocumentSignatureService service = getServiceForSignature(remoteParameters);
        DSSDocument dssDocument = createDSSDocument(remoteDocument);
        DSSDocument extendDocument = service.extendDocument(dssDocument, parameters);
        LOG.info("ExtendDocument is finished");
        return extendDocument;
    }

}
