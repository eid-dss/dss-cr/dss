/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.model;

import java.util.Objects;

/**
 *
 * @author esolis
 */
public class TestResponse {

    private String response;
    private String idTransaction;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(String idTransaction) {
        this.idTransaction = idTransaction;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.response);
        hash = 37 * hash + Objects.hashCode(this.idTransaction);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TestResponse other = (TestResponse) obj;
        if (!Objects.equals(this.response, other.response)) {
            return false;
        }
        if (!Objects.equals(this.idTransaction, other.idTransaction)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TestResponse{" + "response=" + response + ", idTransaction=" + idTransaction + '}';
    }

}
