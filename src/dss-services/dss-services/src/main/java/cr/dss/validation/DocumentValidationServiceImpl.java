/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.validation;

//import cr.dss.controller.tsl.TslServiceImpl;
import eu.europa.esig.dss.DSSDocument;
import eu.europa.esig.dss.InMemoryDocument;
import eu.europa.esig.dss.RemoteDocument;
import eu.europa.esig.dss.utils.Utils;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.validation.SignedDocumentValidator;
import eu.europa.esig.dss.validation.reports.Reports;
import eu.europa.esig.dss.validation.reports.dto.ReportsDTO;
import eu.europa.esig.jaxb.policy.ConstraintsParameters;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author esolis
 */
@Service
public class DocumentValidationServiceImpl {

    private static final Logger LOG = Logger.getLogger(DocumentValidationServiceImpl.class.getName());

    @Autowired
    private CertificateVerifier certificateVerifier;

    @PostConstruct
    public void init() {
//        certificateVerifier = tslServiceImpl.getCertificateVerifier();
    }

    public Reports validateDocument(RemoteDocument signedFile, RemoteDocument originalFile, ConstraintsParameters policy) {
        LOG.info("validateDocument in process...");
        DSSDocument signedDocument = new InMemoryDocument(signedFile.getBytes(), signedFile.getName(), signedFile.getMimeType());
        SignedDocumentValidator signedDocValidator = SignedDocumentValidator.fromDocument(signedDocument);
        signedDocValidator.setCertificateVerifier(certificateVerifier);

        if (originalFile != null && Utils.isArrayNotEmpty(originalFile.getBytes())) {
            List<DSSDocument> list = new ArrayList<>();
            DSSDocument orignalDocument = new InMemoryDocument(originalFile.getBytes(), originalFile.getName(), originalFile.getMimeType());
            list.add(orignalDocument);
            signedDocValidator.setDetachedContents(list);
        }

        Reports reports = policy != null ? signedDocValidator.validateDocument(policy) : signedDocValidator.validateDocument();

//        ReportsDTO result = new ReportsDTO(reports.getDiagnosticDataJaxb(), reports.getSimpleReportJaxb(), reports.getDetailedReportJaxb());

        LOG.info("validateDocument is finished");
        return reports;
    }
}
