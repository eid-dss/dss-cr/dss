package cr.dss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DssServicesApplication {

    public static void main(String[] args) {

        SpringApplication.run(DssServicesApplication.class, args);

    }

}
