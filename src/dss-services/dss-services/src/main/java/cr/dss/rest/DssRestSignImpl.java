/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cr.dss.audit.domain.Event;
import cr.dss.audit.domain.EventDetail;
import cr.dss.audit.domain.IEvent;
import cr.dss.controller.RemoteDocumentSignatureService;
import cr.dss.signature.DocumentSignatureServiceImpl;
import cr.dss.model.DataToSignRequest;
import cr.dss.model.DataToSignResponse;
import cr.dss.model.Document;
import cr.dss.model.Feedback;
import cr.dss.model.FeedbackStatus;
import cr.dss.model.Response;
import cr.dss.model.SignDocumentRequest;
import cr.dss.model.SignDocumentResponse;
import cr.dss.model.TestRequest;
import cr.dss.utils.ConstraintUtils;
import cr.dss.utils.DssUtils;
import eu.europa.esig.dss.DSSDocument;
import eu.europa.esig.dss.DSSException;
import eu.europa.esig.dss.DSSUtils;
import eu.europa.esig.dss.RemoteCertificate;
import eu.europa.esig.dss.RemoteDocument;
import eu.europa.esig.dss.RemoteSignatureParameters;
import eu.europa.esig.dss.SignatureAlgorithm;
import eu.europa.esig.dss.SignatureValue;
import eu.europa.esig.dss.ToBeSigned;
import eu.europa.esig.dss.utils.Utils;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.xml.bind.DatatypeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author esolis
 */
@RestController
public class DssRestSignImpl implements DssRestSign {

    private static final Logger LOG = Logger.getLogger(DssRestSignImpl.class.getName());

    private static final String ERROR_MESSAGE = "An exception was thrown during the operation.";

    Feedback feedback;
    Response response;
    ResponseEntity<Response> responseEntity;
    EventDetail eventDetail;
    Event event;

    private final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

    @Autowired
    private RemoteDocumentSignatureService remoteDocumentSignatureService;

    @Autowired
    private DocumentSignatureServiceImpl documentSignatureServiceImpl;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Override
    public ResponseEntity<Response> test(@RequestBody TestRequest testRequest) {
        LOG.log(Level.INFO, "Start DssRestSignImpl | test");

        Response response = remoteDocumentSignatureService.test(testRequest);

        LOG.log(Level.INFO, "Start DssRestSignImpl | test");

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> dataToSign(@RequestBody DataToSignRequest params) {

        LOG.log(Level.INFO, "Start DssRestSignImpl | dataToSign");

        LOG.log(Level.INFO, "Request: {0}", params.toString());

        event = new Event();
        eventDetail = new EventDetail();

        event.setEvent(IEvent.EventType.DataToSign);
        event.setIdTransaction(DssUtils.randomStringUUID());

        eventDetail.setRequest(gson.toJson(params));

        Set<ConstraintViolation<DataToSignRequest>> constraintViolations = ConstraintUtils.validateBean(params);

        if (constraintViolations.size() > 0) {

            StringBuilder mensaje = new StringBuilder();

            List<String[]> invalidPropertiesList = ConstraintUtils.getInvalidProperties(constraintViolations);

            for (String[] invalidProperties : invalidPropertiesList) {
                mensaje.append(Arrays.toString(invalidProperties));
            }

            throw new ValidationException(mensaje.toString());

        }

        DataToSignResponse dataToSignResponse = new DataToSignResponse();

        RemoteSignatureParameters remoteSignatureParameters = new RemoteSignatureParameters();

        RemoteCertificate signingCertificate = DssUtils.toRemoteCertificate(params.getSigningCertificate());
        List<RemoteCertificate> certificateChain = DssUtils.toCertificateChain(params.getCertificateChain());

        remoteSignatureParameters.setSigningCertificate(signingCertificate);
        remoteSignatureParameters.setCertificateChain(certificateChain);

        remoteSignatureParameters.setSignWithExpiredCertificate(params.isSignWithExpiredCertificate());
        remoteSignatureParameters.setSignatureLevel(params.getSignatureLevel());
        remoteSignatureParameters.setSignaturePackaging(params.getSignaturePackaging());
        remoteSignatureParameters.setEncryptionAlgorithm(params.getEncryptionAlgorithm());
        remoteSignatureParameters.setDigestAlgorithm(params.getDigestAlgorithm());

        remoteSignatureParameters.setBLevelParams(params.getBLevelParams());

        remoteSignatureParameters.setSignatureTimestampParameters(params.getArchiveTimestampParameters());
        remoteSignatureParameters.setSignatureTimestampParameters(params.getSignatureTimestampParameters());
        remoteSignatureParameters.setArchiveTimestampParameters(params.getArchiveTimestampParameters());

        Document document = params.getDocument();
        byte[] docToSign = Utils.fromBase64(document.getEncodedDocument());

        RemoteDocument remoteDocument = new RemoteDocument(docToSign, document.getMimeType(), document.getName());

        try {
            ToBeSigned toBeSigned = documentSignatureServiceImpl.getDataToSign(remoteDocument, remoteSignatureParameters);

            dataToSignResponse.setDataToSign(DatatypeConverter.printBase64Binary(toBeSigned.getBytes()));

            response = new Response<>(dataToSignResponse);

            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

            eventDetail.setResponse(gson.toJson(dataToSignResponse));

            event.setStatus(IEvent.StatusType.Success);

        } catch (DSSException ex) {
            LOG.log(Level.SEVERE, null, ex);

            feedback = new Feedback(FeedbackStatus.EXCEPTION, ex.getLocalizedMessage());
            response = new Response<>(ex, "exception", ERROR_MESSAGE, feedback);

            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

            eventDetail.setResponse(gson.toJson(feedback));

            event.setStatus(IEvent.StatusType.Fail);
        } catch (ValidationException ex) {
            LOG.log(Level.SEVERE, null, ex);

            feedback = new Feedback(FeedbackStatus.EXCEPTION, ex.getLocalizedMessage());
            response = new Response<>(ex, "exception", ERROR_MESSAGE, feedback);

            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

            eventDetail.setResponse(gson.toJson(feedback));

            event.setStatus(IEvent.StatusType.Fail);
        }

        LOG.log(Level.INFO, "Response: {0}", responseEntity.toString());

        LOG.log(Level.INFO, "End DssRestSignImpl | dataToSign");

        event.setCreationTime(new Date());
        event.setEventDetail(eventDetail);

        publisher.publishEvent(event);

        return responseEntity;

    }

    @Override
    public ResponseEntity<Response> signDocument(@RequestBody SignDocumentRequest signDocumentRequest) {
        LOG.info("Start DssResource | signDocument");

        LOG.log(Level.INFO, "signDocumentReques: {0}", signDocumentRequest.toString());

        event = new Event();
        eventDetail = new EventDetail();

        event.setEvent(IEvent.EventType.SignDocument);
        event.setIdTransaction(DssUtils.randomStringUUID());

        eventDetail.setRequest(gson.toJson(signDocumentRequest));

        Set<ConstraintViolation<SignDocumentRequest>> constraintViolations = ConstraintUtils.validateBean(signDocumentRequest);

        if (constraintViolations.size() > 0) {

            StringBuilder mensaje = new StringBuilder();

            List<String[]> invalidPropertiesList = ConstraintUtils.getInvalidProperties(constraintViolations);

            for (String[] invalidProperties : invalidPropertiesList) {
                mensaje.append(Arrays.toString(invalidProperties));
            }

            throw new ValidationException(mensaje.toString());

        }

        RemoteSignatureParameters remoteSignatureParameters = new RemoteSignatureParameters();

        RemoteCertificate signingCertificate = DssUtils.toRemoteCertificate(signDocumentRequest.getSigningCertificate());
        List<RemoteCertificate> certificateChain = DssUtils.toCertificateChain(signDocumentRequest.getCertificateChain());

        remoteSignatureParameters.setSigningCertificate(signingCertificate);
        remoteSignatureParameters.setCertificateChain(certificateChain);

        remoteSignatureParameters.setSignWithExpiredCertificate(signDocumentRequest.isSignWithExpiredCertificate());
        remoteSignatureParameters.setSignatureLevel(signDocumentRequest.getSignatureLevel());
        remoteSignatureParameters.setSignaturePackaging(signDocumentRequest.getSignaturePackaging());
        remoteSignatureParameters.setEncryptionAlgorithm(signDocumentRequest.getEncryptionAlgorithm());
        remoteSignatureParameters.setDigestAlgorithm(signDocumentRequest.getDigestAlgorithm());

        remoteSignatureParameters.setBLevelParams(signDocumentRequest.getBLevelParams());

        remoteSignatureParameters.setSignatureTimestampParameters(signDocumentRequest.getArchiveTimestampParameters());
        remoteSignatureParameters.setSignatureTimestampParameters(signDocumentRequest.getSignatureTimestampParameters());
        remoteSignatureParameters.setArchiveTimestampParameters(signDocumentRequest.getArchiveTimestampParameters());

        Document document = signDocumentRequest.getDocument();
        byte[] docToSign = Utils.fromBase64(document.getEncodedDocument());

        try {

            RemoteDocument remoteDocument = new RemoteDocument(docToSign, document.getMimeType(), document.getName());
            SignatureAlgorithm sigAlgorithm = SignatureAlgorithm.getAlgorithm(remoteSignatureParameters.getEncryptionAlgorithm(), remoteSignatureParameters.getDigestAlgorithm());
            SignatureValue signatureValue = new SignatureValue(sigAlgorithm, DatatypeConverter.parseBase64Binary(signDocumentRequest.getSignatureValue()));

            DSSDocument dssDocument = documentSignatureServiceImpl.signDocument(remoteDocument, remoteSignatureParameters, signatureValue);

            SignDocumentResponse signDocumentResponse = new SignDocumentResponse(DSSUtils.toByteArray(dssDocument), dssDocument.getName(), dssDocument.getMimeType());

            LOG.info("End DssResource | signDocument");

            response = new Response<>(signDocumentResponse);

            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

            eventDetail.setResponse(gson.toJson(signDocumentResponse));

            event.setStatus(IEvent.StatusType.Success);

        } catch (ValidationException ex) {
            LOG.log(Level.SEVERE, null, ex);

            feedback = new Feedback(FeedbackStatus.EXCEPTION, ex.getLocalizedMessage());
            response = new Response<>(ex, "exception", ERROR_MESSAGE, feedback);

            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

            eventDetail.setResponse(gson.toJson(feedback));

            event.setStatus(IEvent.StatusType.Fail);

        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);

            feedback = new Feedback(FeedbackStatus.EXCEPTION, ex.getLocalizedMessage());
            response = new Response<>(ex, "exception", ERROR_MESSAGE, feedback);

            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

            eventDetail.setResponse(gson.toJson(feedback));

            event.setStatus(IEvent.StatusType.Fail);

        }

        LOG.log(Level.INFO, "Response: {0}", responseEntity.toString());

        LOG.info("End DssResource | signDocument");

        event.setCreationTime(new Date());
        event.setEventDetail(eventDetail);

        publisher.publishEvent(event);

        return responseEntity;
    }

}
