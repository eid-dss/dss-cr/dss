/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.model;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author esolis
 */
public class SignDocumentRequest extends AbstractSerializableSignatureParameters implements Serializable {

    @NotNull
    private String signingCertificate;
    
    @NotNull
    private List<String> certificateChain;
    
    @NotNull
    private Document document;
    
    @NotNull
    private String signatureValue;

    public String getSigningCertificate() {
        return signingCertificate;
    }

    public void setSigningCertificate(String signingCertificate) {
        this.signingCertificate = signingCertificate;
    }

    public List<String> getCertificateChain() {
        return certificateChain;
    }

    public void setCertificateChain(List<String> certificateChain) {
        this.certificateChain = certificateChain;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
    
    public String getSignatureValue() {
        return signatureValue;
    }

    public void setSignatureValue(String signatureValue) {
        this.signatureValue = signatureValue;
    }

    @AssertTrue(message = "{error.to.sign.file.mandatory}")
    public boolean isDocumentToSign() {
        return (document != null) && (!document.getEncodedDocument().isEmpty());
    }

    @Override
    public String toString() {
        return "SignDocumentRequest{" + "signingCertificate=" + signingCertificate + ", certificateChain=" + certificateChain + ", document=" + document + ", signatureValue=" + signatureValue + '}';
    }
    
    

}
