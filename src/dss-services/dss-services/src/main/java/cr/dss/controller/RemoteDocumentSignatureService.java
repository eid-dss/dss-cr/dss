/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.controller;

import com.google.common.base.Strings;
import cr.dss.audit.domain.Event;
import cr.dss.audit.domain.EventDetail;
import cr.dss.audit.domain.IEvent;
import cr.dss.model.DataToSignRequest;
import cr.dss.model.Feedback;
import cr.dss.model.FeedbackStatus;
import cr.dss.model.Response;
import cr.dss.model.SignDocumentRequest;
import cr.dss.model.TestRequest;
import cr.dss.model.TestResponse;
import cr.dss.signature.DocumentSignatureServiceImpl;
import cr.dss.utils.ConstraintUtils;
import cr.dss.utils.DssUtils;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author esolis
 */
@Service
public class RemoteDocumentSignatureService implements IRemoteDocumentSignatureService {

    private static final Logger LOG = Logger.getLogger(RemoteDocumentSignatureService.class.getName());

    private static final String ERROR_MESSAGE = "An exception was thrown during the operation.";

    @Autowired
    private DocumentSignatureServiceImpl documentSignatureServiceImpl;

    @Autowired
    private ApplicationEventPublisher publisher;

    Feedback feedback;
    Response response;
    EventDetail eventDetail;
    Event event;

    @Override
    public Response test(TestRequest testRequest) {
        LOG.log(Level.INFO, "Start RemoteDocumentSignatureService | test");

        TestResponse testResponse = new TestResponse();
        testResponse.setResponse("UP");

        event = new Event();
        eventDetail = new EventDetail();

        if (Strings.isNullOrEmpty(testRequest.getIdTransaction())) {
            testRequest.setIdTransaction(DssUtils.randomStringUUID());
        }
        
        event.setEvent(IEvent.EventType.Test);
        event.setBusinessApplication(testRequest.getBusinessApplication());
        event.setIdTransaction(testRequest.getIdTransaction());
        eventDetail.setRequest(testRequest.toString());

        try {

            Set<ConstraintViolation<TestRequest>> constraintViolations = ConstraintUtils.validateBean(testRequest);

            if (constraintViolations.size() > 0) {

                StringBuilder mensaje = new StringBuilder();

                List<String[]> invalidPropertiesList = ConstraintUtils.getInvalidProperties(constraintViolations);

                for (String[] invalidProperties : invalidPropertiesList) {
                    mensaje.append(Arrays.toString(invalidProperties));
                }

                throw new ValidationException(mensaje.toString());

            }

            testResponse.setIdTransaction(testRequest.getIdTransaction());
            response = new Response(testResponse);

            eventDetail.setResponse(response.toString());

            event.setStatus(IEvent.StatusType.Success);

        } catch (ValidationException ex) {
//            LOG.log(Level.SEVERE, null, ex);

            feedback = new Feedback(FeedbackStatus.EXCEPTION, ex.getLocalizedMessage());
            response = new Response<>(ex.getMessage(), "exception", ERROR_MESSAGE, feedback);

            eventDetail.setResponse(feedback.toString());
            event.setStatus(IEvent.StatusType.Fail);
        }

        event.setCreationTime(new Date());
        event.setEventDetail(eventDetail);

        publisher.publishEvent(event);

        LOG.log(Level.INFO, "End RemoteDocumentSignatureService | test");

        return response;
    }

    @Override
    public Response dataToSign(DataToSignRequest dataToSignRequest) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Response signDocument(SignDocumentRequest signDocumentRequest) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
