/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.model;

import eu.europa.esig.dss.MimeType;
import eu.europa.esig.dss.utils.Utils;

/**
 *
 * @author esolis
 */
public final class SignDocumentResponse {

    private String signedDocument;
    private String name;
    private MimeType mimeType;

    public SignDocumentResponse(byte[] signedDocumentByte, String name, MimeType mimeType) {
        this.signedDocument = getBase64Encoded(signedDocumentByte);
        this.name = name;
        this.mimeType = mimeType;
    }

    public String getSignedDocument() {
        return signedDocument;
    }

    public void setSignedDocument(byte[] signedDocumentByte) {
        this.signedDocument = getBase64Encoded(signedDocumentByte);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MimeType getMimeType() {
        return mimeType;
    }

    public void setMimeType(MimeType mimeType) {
        this.mimeType = mimeType;
    }

    public String getBase64Encoded(byte[] signedDocumentByte) {
        return Utils.toBase64(signedDocumentByte);
    }

    @Override
    public String toString() {
        return "SignDocumentResponse{" + "signedDocument=" + signedDocument + ", name=" + name + ", mimeType=" + mimeType + '}';
    }

    
}
