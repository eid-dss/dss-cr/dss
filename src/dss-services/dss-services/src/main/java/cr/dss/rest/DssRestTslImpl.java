/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.dss.rest;

import cr.dss.rest.DssRestTsl;
import cr.dss.model.Feedback;
import cr.dss.model.Response;
import eu.europa.esig.dss.tsl.TLInfo;
import eu.europa.esig.dss.tsl.TSLValidationModel;
import eu.europa.esig.dss.tsl.service.TSLRepository;
import eu.europa.esig.dss.utils.Utils;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author esolis
 */
@RestController
public class DssRestTslImpl implements DssRestTsl {

    private static final Logger LOG = Logger.getLogger(DssRestTslImpl.class.getName());

    Feedback feedback;
    Response response;
    ResponseEntity<Response> responseEntity;

    @Autowired
    private TSLRepository tslRepository;

    @Override
    public ResponseEntity<Response> getSummary() {
        LOG.log(Level.INFO, "Start DssRestTslImpl | getSummary");

        Map<String, TLInfo> tslSummary = new HashMap<>();
        tslSummary = tslRepository.getSummary();

        response = new Response<>(tslSummary);

        responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

        LOG.log(Level.INFO, "Response: {0}", responseEntity.toString());

        LOG.log(Level.INFO, "End DssRestTslImpl | getSummary");

        return responseEntity;
    }

    @Override
    public ResponseEntity<Response> getByCountry(@PathVariable("country") String countryCode) {
        
        LOG.log(Level.INFO, "Start DssRestTslImpl | getByCountry");
        
        LOG.log(Level.INFO, "Request: {0}", countryCode);
        
        String countryUppercase = Utils.upperCase(countryCode);
        TSLValidationModel tslValidationModel = new TSLValidationModel();
        tslValidationModel = tslRepository.getByCountry(countryUppercase);
        
        response = new Response<>(tslValidationModel);

        responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

        LOG.log(Level.INFO, "Response: {0}", responseEntity.toString());

        LOG.log(Level.INFO, "End DssRestTslImpl | getByCountry");

        return responseEntity;
        
    }

}
