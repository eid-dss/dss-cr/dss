package lu.nowina.nexu.object.model;

import eu.europa.esig.dss.DigestAlgorithm;

import eu.europa.esig.dss.EncryptionAlgorithm;

import eu.europa.esig.dss.SignatureAlgorithm;

import java.util.ArrayList;

import java.util.HashMap;

import java.util.List;

import java.util.Map;

import javax.annotation.Generated;

import lu.nowina.nexu.api.EnvironmentInfo;

import lu.nowina.nexu.api.FeedbackStatus;

import lu.nowina.nexu.object.model.GetIdentityInfoResponse.Gender;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-24T17:23:40-0600",

    comments = "version: 1.1.0.Final, compiler: javac, environment: Java 1.8.0_102 (Oracle Corporation)"

)

public class FromPublicObjectModelToAPITestMapperImpl implements FromPublicObjectModelToAPITestMapper {

    private final UtilMappers utilMappers = new UtilMappers();

    @Override

    public lu.nowina.nexu.api.GetCertificateRequest mapGetCertificateRequest(GetCertificateRequest req) {

        if ( req == null ) {

            return null;
        }

        lu.nowina.nexu.api.GetCertificateRequest getCertificateRequest = new lu.nowina.nexu.api.GetCertificateRequest();

        getCertificateRequest.setUserLocale( req.getUserLocale() );

        getCertificateRequest.setExternalId( req.getExternalId() );

        getCertificateRequest.setRequestSeal( req.getRequestSeal() );

        getCertificateRequest.setNonce( req.getNonce() );

        getCertificateRequest.setCertificateFilter( mapCertificateFilter( req.getCertificateFilter() ) );

        return getCertificateRequest;
    }

    @Override

    public lu.nowina.nexu.api.GetCertificateResponse mapGetCertificateResponse(GetCertificateResponse resp) {

        if ( resp == null ) {

            return null;
        }

        lu.nowina.nexu.api.GetCertificateResponse getCertificateResponse = new lu.nowina.nexu.api.GetCertificateResponse();

        getCertificateResponse.setTokenId( mapTokenId( resp.getTokenId() ) );

        getCertificateResponse.setKeyId( resp.getKeyId() );

        getCertificateResponse.setCertificate( utilMappers.mapCertificateToken( resp.getCertificate() ) );

        getCertificateResponse.setCertificateChain( utilMappers.mapCertificateTokens( resp.getCertificateChain() ) );

        if ( resp.getEncryptionAlgorithm() != null ) {

            getCertificateResponse.setEncryptionAlgorithm( Enum.valueOf( EncryptionAlgorithm.class, resp.getEncryptionAlgorithm() ) );
        }

        List<DigestAlgorithm> list = stringListToDigestAlgorithmList( resp.getSupportedDigests() );

        if ( list != null ) {

            getCertificateResponse.setSupportedDigests( list );
        }

        if ( resp.getPreferredDigest() != null ) {

            getCertificateResponse.setPreferredDigest( Enum.valueOf( DigestAlgorithm.class, resp.getPreferredDigest() ) );
        }

        return getCertificateResponse;
    }

    @Override

    public lu.nowina.nexu.api.Execution<lu.nowina.nexu.api.GetCertificateResponse> mapGetCertificateResponse(Execution<GetCertificateResponse> resp, lu.nowina.nexu.api.Execution<lu.nowina.nexu.api.GetCertificateResponse> to) {

        if ( resp == null ) {

            return null;
        }

        to.setFeedback( mapFeedback( resp.getFeedback() ) );

        return to;
    }

    @Override

    public lu.nowina.nexu.api.SignatureRequest mapSignatureRequest(SignatureRequest req) {

        if ( req == null ) {

            return null;
        }

        lu.nowina.nexu.api.SignatureRequest signatureRequest = new lu.nowina.nexu.api.SignatureRequest();

        signatureRequest.setUserLocale( req.getUserLocale() );

        signatureRequest.setExternalId( req.getExternalId() );

        signatureRequest.setRequestSeal( req.getRequestSeal() );

        signatureRequest.setNonce( req.getNonce() );

        signatureRequest.setTokenId( mapTokenId( req.getTokenId() ) );

        signatureRequest.setToBeSigned( mapToBeSigned( req.getToBeSigned() ) );

        if ( req.getDigestAlgorithm() != null ) {

            signatureRequest.setDigestAlgorithm( Enum.valueOf( DigestAlgorithm.class, req.getDigestAlgorithm() ) );
        }

        signatureRequest.setKeyId( req.getKeyId() );

        return signatureRequest;
    }

    @Override

    public lu.nowina.nexu.api.SignatureResponse mapSignatureResponse(SignatureResponse resp, lu.nowina.nexu.api.SignatureResponse to) {

        if ( resp == null ) {

            return null;
        }

        return to;
    }

    @Override

    public lu.nowina.nexu.api.Execution<lu.nowina.nexu.api.SignatureResponse> mapSignatureResponse(Execution<SignatureResponse> resp, lu.nowina.nexu.api.Execution<lu.nowina.nexu.api.SignatureResponse> to) {

        if ( resp == null ) {

            return null;
        }

        to.setFeedback( mapFeedback( resp.getFeedback() ) );

        return to;
    }

    @Override

    public lu.nowina.nexu.api.GetIdentityInfoRequest mapGetIdentityInfoRequest(GetIdentityInfoRequest req) {

        if ( req == null ) {

            return null;
        }

        lu.nowina.nexu.api.GetIdentityInfoRequest getIdentityInfoRequest = new lu.nowina.nexu.api.GetIdentityInfoRequest();

        getIdentityInfoRequest.setUserLocale( req.getUserLocale() );

        getIdentityInfoRequest.setExternalId( req.getExternalId() );

        getIdentityInfoRequest.setRequestSeal( req.getRequestSeal() );

        getIdentityInfoRequest.setNonce( req.getNonce() );

        getIdentityInfoRequest.setIgnoreIfUnsupported( req.isIgnoreIfUnsupported() );

        return getIdentityInfoRequest;
    }

    @Override

    public lu.nowina.nexu.api.GetIdentityInfoResponse mapGetIdentityInfoResponse(GetIdentityInfoResponse resp) {

        if ( resp == null ) {

            return null;
        }

        lu.nowina.nexu.api.GetIdentityInfoResponse getIdentityInfoResponse = new lu.nowina.nexu.api.GetIdentityInfoResponse();

        getIdentityInfoResponse.setCardDeliveryAuthority( resp.getCardDeliveryAuthority() );

        getIdentityInfoResponse.setCardNumber( resp.getCardNumber() );

        getIdentityInfoResponse.setCardValidityDateBegin( resp.getCardValidityDateBegin() );

        getIdentityInfoResponse.setCardValidityDateEnd( resp.getCardValidityDateEnd() );

        getIdentityInfoResponse.setChipNumber( resp.getChipNumber() );

        getIdentityInfoResponse.setDateOfBirth( resp.getDateOfBirth() );

        getIdentityInfoResponse.setFirstName( resp.getFirstName() );

        getIdentityInfoResponse.setGender( mapGender( resp.getGender() ) );

        getIdentityInfoResponse.setMiddleName( resp.getMiddleName() );

        getIdentityInfoResponse.setLastName( resp.getLastName() );

        getIdentityInfoResponse.setNationality( resp.getNationality() );

        getIdentityInfoResponse.setNationalNumber( resp.getNationalNumber() );

        getIdentityInfoResponse.setNobleCondition( resp.getNobleCondition() );

        getIdentityInfoResponse.setPlaceOfBirth( resp.getPlaceOfBirth() );

        getIdentityInfoResponse.setSpecialStatus( resp.getSpecialStatus() );

        getIdentityInfoResponse.setAddress( resp.getAddress() );

        getIdentityInfoResponse.setPostalCode( resp.getPostalCode() );

        getIdentityInfoResponse.setCity( resp.getCity() );

        getIdentityInfoResponse.setPhoto( utilMappers.mapByteArray( resp.getPhoto() ) );

        getIdentityInfoResponse.setPhotoMimeType( resp.getPhotoMimeType() );

        Map<String, lu.nowina.nexu.api.IdentityInfoSignatureData> map = mapIndentityInfoSignatureData( resp.getSignatureData() );

        if ( map != null ) {

            getIdentityInfoResponse.setSignatureData( map );
        }

        return getIdentityInfoResponse;
    }

    @Override

    public lu.nowina.nexu.api.Execution<lu.nowina.nexu.api.GetIdentityInfoResponse> mapGetIdentityInfoResponse(Execution<GetIdentityInfoResponse> resp, lu.nowina.nexu.api.Execution<lu.nowina.nexu.api.GetIdentityInfoResponse> to) {

        if ( resp == null ) {

            return null;
        }

        to.setFeedback( mapFeedback( resp.getFeedback() ) );

        return to;
    }

    @Override

    public lu.nowina.nexu.api.AuthenticateRequest mapAuthenticateRequest(AuthenticateRequest req) {

        if ( req == null ) {

            return null;
        }

        lu.nowina.nexu.api.AuthenticateRequest authenticateRequest = new lu.nowina.nexu.api.AuthenticateRequest();

        authenticateRequest.setUserLocale( req.getUserLocale() );

        authenticateRequest.setExternalId( req.getExternalId() );

        authenticateRequest.setRequestSeal( req.getRequestSeal() );

        authenticateRequest.setNonce( req.getNonce() );

        authenticateRequest.setChallenge( mapToBeSigned( req.getChallenge() ) );

        return authenticateRequest;
    }

    @Override

    public lu.nowina.nexu.api.AuthenticateResponse mapAuthenticateResponse(AuthenticateResponse resp, lu.nowina.nexu.api.AuthenticateResponse to) {

        if ( resp == null ) {

            return null;
        }

        return to;
    }

    @Override

    public lu.nowina.nexu.api.Execution<lu.nowina.nexu.api.AuthenticateResponse> mapAuthenticateResponse(Execution<AuthenticateResponse> resp, lu.nowina.nexu.api.Execution<lu.nowina.nexu.api.AuthenticateResponse> to) {

        if ( resp == null ) {

            return null;
        }

        to.setFeedback( mapFeedback( resp.getFeedback() ) );

        return to;
    }

    @Override

    public lu.nowina.nexu.api.CertificateFilter mapCertificateFilter(CertificateFilter certificateFilter) {

        if ( certificateFilter == null ) {

            return null;
        }

        lu.nowina.nexu.api.CertificateFilter certificateFilter__ = new lu.nowina.nexu.api.CertificateFilter();

        certificateFilter__.setPurpose( certificateFilter.getPurpose() );

        certificateFilter__.setCertificateSHA1( utilMappers.mapByteArray( certificateFilter.getCertificateSHA1() ) );

        return certificateFilter__;
    }

    @Override

    public lu.nowina.nexu.api.Feedback mapFeedback(Feedback feedback) {

        if ( feedback == null ) {

            return null;
        }

        lu.nowina.nexu.api.Feedback feedback_____ = new lu.nowina.nexu.api.Feedback();

        feedback_____.setFeedbackStatus( mapFeedbackStatus( feedback.getFeedbackStatus() ) );

        feedback_____.setStacktrace( feedback.getStacktrace() );

        feedback_____.setUserComment( feedback.getUserComment() );

        feedback_____.setInfo( mapEnvironmentInfo( feedback.getInfo() ) );

        feedback_____.setNexuVersion( feedback.getNexuVersion() );

        return feedback_____;
    }

    @Override

    public lu.nowina.nexu.api.TokenId mapTokenId(TokenId tokenId) {

        if ( tokenId == null ) {

            return null;
        }

        lu.nowina.nexu.api.TokenId tokenId___ = new lu.nowina.nexu.api.TokenId();

        tokenId___.setId( tokenId.getId() );

        return tokenId___;
    }

    @Override

    public eu.europa.esig.dss.ToBeSigned mapToBeSigned(ToBeSigned toBeSigned) {

        if ( toBeSigned == null ) {

            return null;
        }

        eu.europa.esig.dss.ToBeSigned toBeSigned___ = new eu.europa.esig.dss.ToBeSigned();

        toBeSigned___.setBytes( utilMappers.mapByteArray( toBeSigned.getBytes() ) );

        return toBeSigned___;
    }

    @Override

    public FeedbackStatus mapFeedbackStatus(lu.nowina.nexu.object.model.FeedbackStatus feedbackStatus) {

        if ( feedbackStatus == null ) {

            return null;
        }

        FeedbackStatus feedbackStatus__;

        switch ( feedbackStatus ) {

            case NO_PRODUCT_FOUND: feedbackStatus__ = FeedbackStatus.NO_PRODUCT_FOUND;

            break;

            case PRODUCT_NOT_SUPPORTED: feedbackStatus__ = FeedbackStatus.PRODUCT_NOT_SUPPORTED;

            break;

            case NO_KEYS: feedbackStatus__ = FeedbackStatus.NO_KEYS;

            break;

            case EXCEPTION: feedbackStatus__ = FeedbackStatus.EXCEPTION;

            break;

            case SUCCESS: feedbackStatus__ = FeedbackStatus.SUCCESS;

            break;

            case FAILED: feedbackStatus__ = FeedbackStatus.FAILED;

            break;

            case SIGNATURE_VERIFICATION_FAILED: feedbackStatus__ = FeedbackStatus.SIGNATURE_VERIFICATION_FAILED;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + feedbackStatus );
        }

        return feedbackStatus__;
    }

    @Override

    public EnvironmentInfo mapEnvironmentInfo(lu.nowina.nexu.object.model.EnvironmentInfo environmentInfo) {

        if ( environmentInfo == null ) {

            return null;
        }

        EnvironmentInfo environmentInfo__ = new EnvironmentInfo();

        environmentInfo__.setJreVendor( mapJREVendor( environmentInfo.getJreVendor() ) );

        environmentInfo__.setOsVersion( environmentInfo.getOsVersion() );

        environmentInfo__.setArch( mapArch( environmentInfo.getArch() ) );

        environmentInfo__.setOs( mapOS( environmentInfo.getOs() ) );

        environmentInfo__.setOsName( environmentInfo.getOsName() );

        environmentInfo__.setOsArch( environmentInfo.getOsArch() );

        return environmentInfo__;
    }

    @Override

    public lu.nowina.nexu.api.JREVendor mapJREVendor(JREVendor jreVendor) {

        if ( jreVendor == null ) {

            return null;
        }

        lu.nowina.nexu.api.JREVendor jREVendor_;

        switch ( jreVendor ) {

            case ORACLE: jREVendor_ = lu.nowina.nexu.api.JREVendor.ORACLE;

            break;

            case NOT_RECOGNIZED: jREVendor_ = lu.nowina.nexu.api.JREVendor.NOT_RECOGNIZED;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + jreVendor );
        }

        return jREVendor_;
    }

    @Override

    public lu.nowina.nexu.api.Arch mapArch(Arch arch) {

        if ( arch == null ) {

            return null;
        }

        lu.nowina.nexu.api.Arch arch__;

        switch ( arch ) {

            case X86: arch__ = lu.nowina.nexu.api.Arch.X86;

            break;

            case AMD64: arch__ = lu.nowina.nexu.api.Arch.AMD64;

            break;

            case NOT_RECOGNIZED: arch__ = lu.nowina.nexu.api.Arch.NOT_RECOGNIZED;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + arch );
        }

        return arch__;
    }

    @Override

    public lu.nowina.nexu.api.OS mapOS(OS os) {

        if ( os == null ) {

            return null;
        }

        lu.nowina.nexu.api.OS oS_;

        switch ( os ) {

            case MACOSX: oS_ = lu.nowina.nexu.api.OS.MACOSX;

            break;

            case LINUX: oS_ = lu.nowina.nexu.api.OS.LINUX;

            break;

            case WINDOWS: oS_ = lu.nowina.nexu.api.OS.WINDOWS;

            break;

            case NOT_RECOGNIZED: oS_ = lu.nowina.nexu.api.OS.NOT_RECOGNIZED;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + os );
        }

        return oS_;
    }

    @Override

    public lu.nowina.nexu.api.Purpose mapPurpose(Purpose purpose) {

        if ( purpose == null ) {

            return null;
        }

        lu.nowina.nexu.api.Purpose purpose_;

        switch ( purpose ) {

            case SIGNATURE: purpose_ = lu.nowina.nexu.api.Purpose.SIGNATURE;

            break;

            case AUTHENTICATION: purpose_ = lu.nowina.nexu.api.Purpose.AUTHENTICATION;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + purpose );
        }

        return purpose_;
    }

    @Override

    public lu.nowina.nexu.api.GetIdentityInfoResponse.Gender mapGender(Gender gender) {

        if ( gender == null ) {

            return null;
        }

        lu.nowina.nexu.api.GetIdentityInfoResponse.Gender gender__;

        switch ( gender ) {

            case MALE: gender__ = lu.nowina.nexu.api.GetIdentityInfoResponse.Gender.MALE;

            break;

            case FEMALE: gender__ = lu.nowina.nexu.api.GetIdentityInfoResponse.Gender.FEMALE;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + gender );
        }

        return gender__;
    }

    @Override

    public eu.europa.esig.dss.SignatureValue mapSignatureValue(SignatureValue signatureValue) {

        if ( signatureValue == null ) {

            return null;
        }

        eu.europa.esig.dss.SignatureValue signatureValue_ = new eu.europa.esig.dss.SignatureValue();

        if ( signatureValue.getAlgorithm() != null ) {

            signatureValue_.setAlgorithm( Enum.valueOf( SignatureAlgorithm.class, signatureValue.getAlgorithm() ) );
        }

        signatureValue_.setValue( utilMappers.mapByteArray( signatureValue.getValue() ) );

        return signatureValue_;
    }

    @Override

    public lu.nowina.nexu.api.IdentityInfoSignatureData mapIndentityInfoSignatureData(IdentityInfoSignatureData iisd) {

        if ( iisd == null ) {

            return null;
        }

        lu.nowina.nexu.api.IdentityInfoSignatureData identityInfoSignatureData = new lu.nowina.nexu.api.IdentityInfoSignatureData();

        identityInfoSignatureData.setRawData( utilMappers.mapByteArray( iisd.getRawData() ) );

        identityInfoSignatureData.setSignatureValue( mapSignatureValue( iisd.getSignatureValue() ) );

        identityInfoSignatureData.setCertificateChain( utilMappers.mapCertificateTokens( iisd.getCertificateChain() ) );

        return identityInfoSignatureData;
    }

    @Override

    public Map<String, lu.nowina.nexu.api.IdentityInfoSignatureData> mapIndentityInfoSignatureData(Map<String, IdentityInfoSignatureData> map) {

        if ( map == null ) {

            return null;
        }

        Map<String, lu.nowina.nexu.api.IdentityInfoSignatureData> map__ = new HashMap<String, lu.nowina.nexu.api.IdentityInfoSignatureData>();

        for ( java.util.Map.Entry<String, IdentityInfoSignatureData> entry : map.entrySet() ) {

            String key = entry.getKey();

            lu.nowina.nexu.api.IdentityInfoSignatureData value = mapIndentityInfoSignatureData( entry.getValue() );

            map__.put( key, value );
        }

        return map__;
    }

    protected List<DigestAlgorithm> stringListToDigestAlgorithmList(List<String> list) {

        if ( list == null ) {

            return null;
        }

        List<DigestAlgorithm> list_ = new ArrayList<DigestAlgorithm>();

        for ( String string : list ) {

            list_.add( Enum.valueOf( DigestAlgorithm.class, string ) );
        }

        return list_;
    }
}

