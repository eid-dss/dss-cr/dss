package lu.nowina.nexu.object.model;

import eu.europa.esig.dss.DigestAlgorithm;

import eu.europa.esig.dss.SignatureValue;

import eu.europa.esig.dss.ToBeSigned;

import java.util.ArrayList;

import java.util.HashMap;

import java.util.List;

import java.util.Map;

import javax.annotation.Generated;

import lu.nowina.nexu.api.Arch;

import lu.nowina.nexu.api.AuthenticateRequest;

import lu.nowina.nexu.api.AuthenticateResponse;

import lu.nowina.nexu.api.CertificateFilter;

import lu.nowina.nexu.api.EnvironmentInfo;

import lu.nowina.nexu.api.Execution;

import lu.nowina.nexu.api.Feedback;

import lu.nowina.nexu.api.FeedbackStatus;

import lu.nowina.nexu.api.GetCertificateRequest;

import lu.nowina.nexu.api.GetCertificateResponse;

import lu.nowina.nexu.api.GetIdentityInfoRequest;

import lu.nowina.nexu.api.GetIdentityInfoResponse;

import lu.nowina.nexu.api.GetIdentityInfoResponse.Gender;

import lu.nowina.nexu.api.IdentityInfoSignatureData;

import lu.nowina.nexu.api.JREVendor;

import lu.nowina.nexu.api.OS;

import lu.nowina.nexu.api.Purpose;

import lu.nowina.nexu.api.SignatureRequest;

import lu.nowina.nexu.api.SignatureResponse;

import lu.nowina.nexu.api.TokenId;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-24T17:23:41-0600",

    comments = "version: 1.1.0.Final, compiler: javac, environment: Java 1.8.0_102 (Oracle Corporation)"

)

public class FromAPIToPublicObjectModelTestMapperImpl implements FromAPIToPublicObjectModelTestMapper {

    private final UtilMappers utilMappers = new UtilMappers();

    @Override

    public lu.nowina.nexu.object.model.GetCertificateRequest mapGetCertificateRequest(GetCertificateRequest req) {

        if ( req == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.GetCertificateRequest getCertificateRequest = new lu.nowina.nexu.object.model.GetCertificateRequest();

        getCertificateRequest.setUserLocale( req.getUserLocale() );

        getCertificateRequest.setExternalId( req.getExternalId() );

        getCertificateRequest.setRequestSeal( req.getRequestSeal() );

        getCertificateRequest.setNonce( req.getNonce() );

        getCertificateRequest.setCertificateFilter( mapCertificateFilter( req.getCertificateFilter() ) );

        return getCertificateRequest;
    }

    @Override

    public lu.nowina.nexu.object.model.GetCertificateResponse mapGetCertificateResponse(GetCertificateResponse resp) {

        if ( resp == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.GetCertificateResponse getCertificateResponse = new lu.nowina.nexu.object.model.GetCertificateResponse();

        getCertificateResponse.setTokenId( mapTokenId( resp.getTokenId() ) );

        getCertificateResponse.setKeyId( resp.getKeyId() );

        getCertificateResponse.setCertificate( utilMappers.mapCertificateToken( resp.getCertificate() ) );

        getCertificateResponse.setCertificateChain( utilMappers.mapCertificateTokens( resp.getCertificateChain() ) );

        if ( resp.getEncryptionAlgorithm() != null ) {

            getCertificateResponse.setEncryptionAlgorithm( resp.getEncryptionAlgorithm().name() );
        }

        List<String> list = digestAlgorithmListToStringList( resp.getSupportedDigests() );

        if ( list != null ) {

            getCertificateResponse.setSupportedDigests( list );
        }

        if ( resp.getPreferredDigest() != null ) {

            getCertificateResponse.setPreferredDigest( resp.getPreferredDigest().name() );
        }

        return getCertificateResponse;
    }

    @Override

    public lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.GetCertificateResponse> mapGetCertificateResponse(Execution<GetCertificateResponse> resp) {

        if ( resp == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.GetCertificateResponse> execution = new lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.GetCertificateResponse>();

        execution.setFeedback( mapFeedback( resp.getFeedback() ) );

        execution.setSuccess( resp.isSuccess() );

        execution.setResponse( mapGetCertificateResponse( resp.getResponse() ) );

        execution.setError( resp.getError() );

        execution.setErrorMessage( resp.getErrorMessage() );

        return execution;
    }

    @Override

    public lu.nowina.nexu.object.model.SignatureRequest mapSignatureRequest(SignatureRequest req) {

        if ( req == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.SignatureRequest signatureRequest = new lu.nowina.nexu.object.model.SignatureRequest();

        signatureRequest.setUserLocale( req.getUserLocale() );

        signatureRequest.setExternalId( req.getExternalId() );

        signatureRequest.setRequestSeal( req.getRequestSeal() );

        signatureRequest.setNonce( req.getNonce() );

        signatureRequest.setTokenId( mapTokenId( req.getTokenId() ) );

        signatureRequest.setToBeSigned( mapToBeSigned( req.getToBeSigned() ) );

        if ( req.getDigestAlgorithm() != null ) {

            signatureRequest.setDigestAlgorithm( req.getDigestAlgorithm().name() );
        }

        signatureRequest.setKeyId( req.getKeyId() );

        return signatureRequest;
    }

    @Override

    public lu.nowina.nexu.object.model.SignatureResponse mapSignatureResponse(SignatureResponse resp) {

        if ( resp == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.SignatureResponse signatureResponse = new lu.nowina.nexu.object.model.SignatureResponse();

        signatureResponse.setSignatureValue( utilMappers.mapByteArray( resp.getSignatureValue() ) );

        if ( resp.getSignatureAlgorithm() != null ) {

            signatureResponse.setSignatureAlgorithm( resp.getSignatureAlgorithm().name() );
        }

        signatureResponse.setCertificate( utilMappers.mapCertificateToken( resp.getCertificate() ) );

        signatureResponse.setCertificateChain( utilMappers.mapCertificateTokens( resp.getCertificateChain() ) );

        return signatureResponse;
    }

    @Override

    public lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.SignatureResponse> mapSignatureResponse(Execution<SignatureResponse> resp) {

        if ( resp == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.SignatureResponse> execution = new lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.SignatureResponse>();

        execution.setFeedback( mapFeedback( resp.getFeedback() ) );

        execution.setSuccess( resp.isSuccess() );

        execution.setResponse( mapSignatureResponse( resp.getResponse() ) );

        execution.setError( resp.getError() );

        execution.setErrorMessage( resp.getErrorMessage() );

        return execution;
    }

    @Override

    public lu.nowina.nexu.object.model.GetIdentityInfoRequest mapGetIdentityInfoRequest(GetIdentityInfoRequest req) {

        if ( req == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.GetIdentityInfoRequest getIdentityInfoRequest = new lu.nowina.nexu.object.model.GetIdentityInfoRequest();

        getIdentityInfoRequest.setUserLocale( req.getUserLocale() );

        getIdentityInfoRequest.setExternalId( req.getExternalId() );

        getIdentityInfoRequest.setRequestSeal( req.getRequestSeal() );

        getIdentityInfoRequest.setNonce( req.getNonce() );

        getIdentityInfoRequest.setIgnoreIfUnsupported( req.isIgnoreIfUnsupported() );

        return getIdentityInfoRequest;
    }

    @Override

    public lu.nowina.nexu.object.model.GetIdentityInfoResponse mapGetIdentityInfoResponse(GetIdentityInfoResponse resp) {

        if ( resp == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.GetIdentityInfoResponse getIdentityInfoResponse = new lu.nowina.nexu.object.model.GetIdentityInfoResponse();

        getIdentityInfoResponse.setCardDeliveryAuthority( resp.getCardDeliveryAuthority() );

        getIdentityInfoResponse.setCardNumber( resp.getCardNumber() );

        getIdentityInfoResponse.setCardValidityDateBegin( resp.getCardValidityDateBegin() );

        getIdentityInfoResponse.setCardValidityDateEnd( resp.getCardValidityDateEnd() );

        getIdentityInfoResponse.setChipNumber( resp.getChipNumber() );

        getIdentityInfoResponse.setDateOfBirth( resp.getDateOfBirth() );

        getIdentityInfoResponse.setFirstName( resp.getFirstName() );

        getIdentityInfoResponse.setGender( mapGender( resp.getGender() ) );

        getIdentityInfoResponse.setMiddleName( resp.getMiddleName() );

        getIdentityInfoResponse.setLastName( resp.getLastName() );

        getIdentityInfoResponse.setNationality( resp.getNationality() );

        getIdentityInfoResponse.setNationalNumber( resp.getNationalNumber() );

        getIdentityInfoResponse.setNobleCondition( resp.getNobleCondition() );

        getIdentityInfoResponse.setPlaceOfBirth( resp.getPlaceOfBirth() );

        getIdentityInfoResponse.setSpecialStatus( resp.getSpecialStatus() );

        getIdentityInfoResponse.setAddress( resp.getAddress() );

        getIdentityInfoResponse.setPostalCode( resp.getPostalCode() );

        getIdentityInfoResponse.setCity( resp.getCity() );

        getIdentityInfoResponse.setPhoto( utilMappers.mapByteArray( resp.getPhoto() ) );

        getIdentityInfoResponse.setPhotoMimeType( resp.getPhotoMimeType() );

        Map<String, lu.nowina.nexu.object.model.IdentityInfoSignatureData> map = mapIndentityInfoSignatureData( resp.getSignatureData() );

        if ( map != null ) {

            getIdentityInfoResponse.setSignatureData( map );
        }

        return getIdentityInfoResponse;
    }

    @Override

    public lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.GetIdentityInfoResponse> mapGetIdentityInfoResponse(Execution<GetIdentityInfoResponse> resp) {

        if ( resp == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.GetIdentityInfoResponse> execution = new lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.GetIdentityInfoResponse>();

        execution.setFeedback( mapFeedback( resp.getFeedback() ) );

        execution.setSuccess( resp.isSuccess() );

        execution.setResponse( mapGetIdentityInfoResponse( resp.getResponse() ) );

        execution.setError( resp.getError() );

        execution.setErrorMessage( resp.getErrorMessage() );

        return execution;
    }

    @Override

    public lu.nowina.nexu.object.model.AuthenticateRequest mapAuthenticateRequest(AuthenticateRequest req) {

        if ( req == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.AuthenticateRequest authenticateRequest = new lu.nowina.nexu.object.model.AuthenticateRequest();

        authenticateRequest.setUserLocale( req.getUserLocale() );

        authenticateRequest.setExternalId( req.getExternalId() );

        authenticateRequest.setRequestSeal( req.getRequestSeal() );

        authenticateRequest.setNonce( req.getNonce() );

        authenticateRequest.setChallenge( mapToBeSigned( req.getChallenge() ) );

        return authenticateRequest;
    }

    @Override

    public lu.nowina.nexu.object.model.AuthenticateResponse mapAuthenticateResponse(AuthenticateResponse resp) {

        if ( resp == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.AuthenticateResponse authenticateResponse = new lu.nowina.nexu.object.model.AuthenticateResponse();

        authenticateResponse.setKeyId( resp.getKeyId() );

        authenticateResponse.setCertificate( utilMappers.mapCertificateToken( resp.getCertificate() ) );

        authenticateResponse.setCertificateChain( utilMappers.mapCertificateTokens( resp.getCertificateChain() ) );

        authenticateResponse.setSignatureValue( mapSignatureValue( resp.getSignatureValue() ) );

        return authenticateResponse;
    }

    @Override

    public lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.AuthenticateResponse> mapAuthenticateResponse(Execution<AuthenticateResponse> resp) {

        if ( resp == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.AuthenticateResponse> execution = new lu.nowina.nexu.object.model.Execution<lu.nowina.nexu.object.model.AuthenticateResponse>();

        execution.setFeedback( mapFeedback( resp.getFeedback() ) );

        execution.setSuccess( resp.isSuccess() );

        execution.setResponse( mapAuthenticateResponse( resp.getResponse() ) );

        execution.setError( resp.getError() );

        execution.setErrorMessage( resp.getErrorMessage() );

        return execution;
    }

    @Override

    public lu.nowina.nexu.object.model.CertificateFilter mapCertificateFilter(CertificateFilter certificateFilter) {

        if ( certificateFilter == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.CertificateFilter certificateFilter__ = new lu.nowina.nexu.object.model.CertificateFilter();

        certificateFilter__.setPurpose( certificateFilter.getPurpose() );

        certificateFilter__.setCertificateSHA1( utilMappers.mapByteArray( certificateFilter.getCertificateSHA1() ) );

        return certificateFilter__;
    }

    @Override

    public lu.nowina.nexu.object.model.Feedback mapFeedback(Feedback feedback) {

        if ( feedback == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.Feedback feedback_____ = new lu.nowina.nexu.object.model.Feedback();

        feedback_____.setFeedbackStatus( mapFeedbackStatus( feedback.getFeedbackStatus() ) );

        feedback_____.setStacktrace( feedback.getStacktrace() );

        feedback_____.setUserComment( feedback.getUserComment() );

        feedback_____.setInfo( mapEnvironmentInfo( feedback.getInfo() ) );

        feedback_____.setNexuVersion( feedback.getNexuVersion() );

        return feedback_____;
    }

    @Override

    public lu.nowina.nexu.object.model.TokenId mapTokenId(TokenId tokenId) {

        if ( tokenId == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.TokenId tokenId___ = new lu.nowina.nexu.object.model.TokenId();

        tokenId___.setId( tokenId.getId() );

        return tokenId___;
    }

    @Override

    public lu.nowina.nexu.object.model.ToBeSigned mapToBeSigned(ToBeSigned toBeSigned) {

        if ( toBeSigned == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.ToBeSigned toBeSigned___ = new lu.nowina.nexu.object.model.ToBeSigned();

        toBeSigned___.setBytes( utilMappers.mapByteArray( toBeSigned.getBytes() ) );

        return toBeSigned___;
    }

    @Override

    public lu.nowina.nexu.object.model.FeedbackStatus mapFeedbackStatus(FeedbackStatus feedbackStatus) {

        if ( feedbackStatus == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.FeedbackStatus feedbackStatus__;

        switch ( feedbackStatus ) {

            case NO_PRODUCT_FOUND: feedbackStatus__ = lu.nowina.nexu.object.model.FeedbackStatus.NO_PRODUCT_FOUND;

            break;

            case PRODUCT_NOT_SUPPORTED: feedbackStatus__ = lu.nowina.nexu.object.model.FeedbackStatus.PRODUCT_NOT_SUPPORTED;

            break;

            case NO_KEYS: feedbackStatus__ = lu.nowina.nexu.object.model.FeedbackStatus.NO_KEYS;

            break;

            case EXCEPTION: feedbackStatus__ = lu.nowina.nexu.object.model.FeedbackStatus.EXCEPTION;

            break;

            case SUCCESS: feedbackStatus__ = lu.nowina.nexu.object.model.FeedbackStatus.SUCCESS;

            break;

            case FAILED: feedbackStatus__ = lu.nowina.nexu.object.model.FeedbackStatus.FAILED;

            break;

            case SIGNATURE_VERIFICATION_FAILED: feedbackStatus__ = lu.nowina.nexu.object.model.FeedbackStatus.SIGNATURE_VERIFICATION_FAILED;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + feedbackStatus );
        }

        return feedbackStatus__;
    }

    @Override

    public lu.nowina.nexu.object.model.EnvironmentInfo mapEnvironmentInfo(EnvironmentInfo environmentInfo) {

        if ( environmentInfo == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.EnvironmentInfo environmentInfo__ = new lu.nowina.nexu.object.model.EnvironmentInfo();

        environmentInfo__.setJreVendor( mapJREVendor( environmentInfo.getJreVendor() ) );

        environmentInfo__.setOsName( environmentInfo.getOsName() );

        environmentInfo__.setOsArch( environmentInfo.getOsArch() );

        environmentInfo__.setOsVersion( environmentInfo.getOsVersion() );

        environmentInfo__.setArch( mapArch( environmentInfo.getArch() ) );

        environmentInfo__.setOs( mapOS( environmentInfo.getOs() ) );

        return environmentInfo__;
    }

    @Override

    public lu.nowina.nexu.object.model.JREVendor mapJREVendor(JREVendor jreVendor) {

        if ( jreVendor == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.JREVendor jREVendor_;

        switch ( jreVendor ) {

            case ORACLE: jREVendor_ = lu.nowina.nexu.object.model.JREVendor.ORACLE;

            break;

            case NOT_RECOGNIZED: jREVendor_ = lu.nowina.nexu.object.model.JREVendor.NOT_RECOGNIZED;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + jreVendor );
        }

        return jREVendor_;
    }

    @Override

    public lu.nowina.nexu.object.model.Arch mapArch(Arch arch) {

        if ( arch == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.Arch arch__;

        switch ( arch ) {

            case X86: arch__ = lu.nowina.nexu.object.model.Arch.X86;

            break;

            case AMD64: arch__ = lu.nowina.nexu.object.model.Arch.AMD64;

            break;

            case NOT_RECOGNIZED: arch__ = lu.nowina.nexu.object.model.Arch.NOT_RECOGNIZED;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + arch );
        }

        return arch__;
    }

    @Override

    public lu.nowina.nexu.object.model.OS mapOS(OS os) {

        if ( os == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.OS oS_;

        switch ( os ) {

            case MACOSX: oS_ = lu.nowina.nexu.object.model.OS.MACOSX;

            break;

            case LINUX: oS_ = lu.nowina.nexu.object.model.OS.LINUX;

            break;

            case WINDOWS: oS_ = lu.nowina.nexu.object.model.OS.WINDOWS;

            break;

            case NOT_RECOGNIZED: oS_ = lu.nowina.nexu.object.model.OS.NOT_RECOGNIZED;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + os );
        }

        return oS_;
    }

    @Override

    public lu.nowina.nexu.object.model.Purpose mapPurpose(Purpose purpose) {

        if ( purpose == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.Purpose purpose_;

        switch ( purpose ) {

            case SIGNATURE: purpose_ = lu.nowina.nexu.object.model.Purpose.SIGNATURE;

            break;

            case AUTHENTICATION: purpose_ = lu.nowina.nexu.object.model.Purpose.AUTHENTICATION;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + purpose );
        }

        return purpose_;
    }

    @Override

    public lu.nowina.nexu.object.model.GetIdentityInfoResponse.Gender mapGender(Gender gender) {

        if ( gender == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.GetIdentityInfoResponse.Gender gender__;

        switch ( gender ) {

            case MALE: gender__ = lu.nowina.nexu.object.model.GetIdentityInfoResponse.Gender.MALE;

            break;

            case FEMALE: gender__ = lu.nowina.nexu.object.model.GetIdentityInfoResponse.Gender.FEMALE;

            break;

            default: throw new IllegalArgumentException( "Unexpected enum constant: " + gender );
        }

        return gender__;
    }

    @Override

    public lu.nowina.nexu.object.model.SignatureValue mapSignatureValue(SignatureValue signatureValue) {

        if ( signatureValue == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.SignatureValue signatureValue__ = new lu.nowina.nexu.object.model.SignatureValue();

        if ( signatureValue.getAlgorithm() != null ) {

            signatureValue__.setAlgorithm( signatureValue.getAlgorithm().name() );
        }

        signatureValue__.setValue( utilMappers.mapByteArray( signatureValue.getValue() ) );

        return signatureValue__;
    }

    @Override

    public lu.nowina.nexu.object.model.IdentityInfoSignatureData mapIndentityInfoSignatureData(IdentityInfoSignatureData iisd) {

        if ( iisd == null ) {

            return null;
        }

        lu.nowina.nexu.object.model.IdentityInfoSignatureData identityInfoSignatureData = new lu.nowina.nexu.object.model.IdentityInfoSignatureData();

        identityInfoSignatureData.setRawData( utilMappers.mapByteArray( iisd.getRawData() ) );

        identityInfoSignatureData.setSignatureValue( mapSignatureValue( iisd.getSignatureValue() ) );

        identityInfoSignatureData.setCertificateChain( utilMappers.mapCertificateTokens( iisd.getCertificateChain() ) );

        return identityInfoSignatureData;
    }

    @Override

    public Map<String, lu.nowina.nexu.object.model.IdentityInfoSignatureData> mapIndentityInfoSignatureData(Map<String, IdentityInfoSignatureData> map) {

        if ( map == null ) {

            return null;
        }

        Map<String, lu.nowina.nexu.object.model.IdentityInfoSignatureData> map__ = new HashMap<String, lu.nowina.nexu.object.model.IdentityInfoSignatureData>();

        for ( java.util.Map.Entry<String, IdentityInfoSignatureData> entry : map.entrySet() ) {

            String key = entry.getKey();

            lu.nowina.nexu.object.model.IdentityInfoSignatureData value = mapIndentityInfoSignatureData( entry.getValue() );

            map__.put( key, value );
        }

        return map__;
    }

    protected List<String> digestAlgorithmListToStringList(List<DigestAlgorithm> list) {

        if ( list == null ) {

            return null;
        }

        List<String> list_ = new ArrayList<String>();

        for ( DigestAlgorithm digestAlgorithm : list ) {

            list_.add( digestAlgorithm.name() );
        }

        return list_;
    }
}

