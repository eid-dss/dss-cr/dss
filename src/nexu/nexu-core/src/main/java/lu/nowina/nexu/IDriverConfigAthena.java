/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lu.nowina.nexu;

/**
 *
 * @author esolis
 */
public interface IDriverConfigAthena {
    
     enum AthenaTerminalLabel {

        Linux("Athena ASE IIIe [SCR Interface] (000000000000) 00 00"), Windows("Athena ASEDrive IIIe USB 0"), None("None");

        private String value;

        private AthenaTerminalLabel(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    
    enum AthenaDriver {

        Linux("/usr/lib/x64-athena/libASEP11.so"), Windows(System.getenv("HOMEDRIVE") + "\\windows\\system32\\asepkcs.dll"), None("None");

        private String value;

        private AthenaDriver(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    
}
