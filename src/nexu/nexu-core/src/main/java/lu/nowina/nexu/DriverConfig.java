/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lu.nowina.nexu;

import lu.nowina.nexu.api.DetectedCard;
import lu.nowina.nexu.api.Match;
import lu.nowina.nexu.api.NexuAPI;
import lu.nowina.nexu.api.OS;
import lu.nowina.nexu.api.ScAPI;
import lu.nowina.nexu.generic.GenericCardAdapter;
import lu.nowina.nexu.generic.SCInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author esolis
 */
public class DriverConfig implements IDriverConfig {
    
    private static final Logger logger = LoggerFactory.getLogger(DriverConfig.class.getSimpleName());

    private final NexuAPI api;
    
    private String apiDriver;
    
    private ScAPI scAPI;

    public DriverConfig(NexuAPI api) {
        this.api = api;
    }

    public DriverProvider driverProvider(String terminalLabel) {

        if ((terminalLabel.equals(IDriverConfigAthena.AthenaTerminalLabel.Linux.getValue())) || (terminalLabel.equals((IDriverConfigAthena.AthenaTerminalLabel.Windows.getValue())))) {
            return DriverProvider.ATHENA;
        } else {
            logger.warn("No configuration available for driver config provider");
            return DriverProvider.NO_DRIVER_PROVIDER;
        }

    }

    public String driverConfigApi(String terminalLabel) {
        OS os = api.getEnvironmentInfo().getOs();

        if (os == OS.LINUX && terminalLabel.equals(IDriverConfigAthena.AthenaTerminalLabel.Linux.getValue())) {
            scAPI = ScAPI.PKCS_11;
            return IDriverConfigAthena.AthenaDriver.Linux.getValue();
        } else if (os == OS.WINDOWS && terminalLabel.equals(IDriverConfigAthena.AthenaTerminalLabel.Windows.getValue())) {
            scAPI = ScAPI.MSCAPI;
            return IDriverConfigAthena.AthenaDriver.Windows.getValue();
        } else {
            logger.warn("No configuration available for driver config Athena");
            return IDriverConfigAthena.AthenaDriver.None.getValue();
        }

    }

    public boolean isdefaultDriverProvider(DetectedCard d) {
        logger.debug("Label: " + d.getLabel());
        switch (driverProvider(d.getTerminalLabel())) {

            case ATHENA:
                apiDriver = driverConfigApi(d.getTerminalLabel());

                if (!apiDriver.equals(IDriverConfigAthena.AthenaDriver.None.getValue())) {
                    return true;
                }
                return false;
            case NO_DRIVER_PROVIDER:
                logger.warn("No defaul driver provider found");
                return false;
        }
        return false;
    }

    public String getApiDriver() {
        return apiDriver;
    }

    public void setApiDriver(String apiDriver) {
        this.apiDriver = apiDriver;
    }

    public ScAPI getScAPI() {
        return scAPI;
    }

    public void setScAPI(ScAPI scAPI) {
        this.scAPI = scAPI;
    }
    
}
